package parser.parse.ParserByWiki;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ParserByWikiApplication {

	public static void main(String[] args) {
		SpringApplication.run(ParserByWikiApplication.class, args);
	}

}
