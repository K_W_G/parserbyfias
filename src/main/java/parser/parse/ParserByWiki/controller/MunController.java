package parser.parse.ParserByWiki.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import parser.parse.ParserByWiki.service.*;

// TODO: 29.02.2024 Возможно лучше разбить логику обработки на несколько сервисов и контроллеров
/**
 * Класс контроллера, отвечающий за логику обработки XML файлов и использующийся для получения данных с API OSM
 */
@RestController
@RequestMapping("/municipal")
public class MunController {
    public ParserXMLService parserXMLService;
    public OSMService osmService;
    public GeneratorWKT generatorWKTService;
    private GeoDataSQL geoDataSQL;

    private LocationsTableSQLService locationsTableSQLService;

    @Autowired
    public MunController(ParserXMLService municipalService,
                         OSMService osmService,
                         GeneratorWKT generatorWKTService,
                         GeoDataSQL geoDataSQL,
                         LocationsTableSQLService locationsTableSQLService) {
        this.parserXMLService = municipalService;
        this.osmService = osmService;
        this.generatorWKTService = generatorWKTService;
        this.geoDataSQL = geoDataSQL;
        this.locationsTableSQLService = locationsTableSQLService;
    }

    @GetMapping("/parseXML")
    public String getMunic() {
        parserXMLService.parseXML();
        parserXMLService.addData();
        return "OK";
    }

    @GetMapping("/getHamlet/{name}")
    public String getPoints(@PathVariable(name = "name") String nameOkrug) throws Exception {
        if (nameOkrug == null || nameOkrug.equals("") || nameOkrug.equals(" ")) {
            return "Вы не ввели муниципальное образование в параметры поиска";
        }
        osmService.getPoints(nameOkrug);
        return "OK";
    }

    @GetMapping("/generateWKTPoints")
    public String generateWKT() throws Exception {
        generatorWKTService.generateWKT();
        return "OK";
    }

    @GetMapping("/getSQLOblastGeoData")
    public String getOblastGeoData() {
        return geoDataSQL.getOblast();
    }

    @GetMapping("/getSQLMoAndGo")
    public String getMoAndGoData() {
        String sql = geoDataSQL.getMoAndGo();
        System.out.println(sql);
        return sql;
    }

    @GetMapping("/getSQLNasel")
    public String getNasel() {
        String sql = geoDataSQL.getNasel();
        System.out.println(sql);
        return sql;
    }

    @GetMapping("/getSQLOblLocations")
    public String getOblastLocationTable() {
        return locationsTableSQLService.getOblast();
    }

    @GetMapping("/getSQLMoAndGoLocations")
    public String getSQLMoAndGoLocations() {
        String sql = locationsTableSQLService.getMoAndGo();
        System.out.println(sql);
        return sql;
    }
    @GetMapping("/getSQLNaselLocations")
    public String getSQLNaselLocations() {
        String sql = locationsTableSQLService.getNasel();
        System.out.println(sql);
        return sql;
    }
}
