package parser.parse.ParserByWiki.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;

/**
 * Класс, использующийся для парсинга файла с началом AS_ADDR_OBJ_.
 * Содержит параметры, соответствующие наименования в каждой сущности OBJECT в файле XML
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@XmlAccessorType(XmlAccessType.FIELD)
public class AddresObjectClass {
    // TODO: 29.02.2024 Убрать параметр id
    /**
     * Идентификатор сущности (По сути не нужен)
     */
    @XmlAttribute(name = "ID")
    private String id;
    /**
     * Объектный идентификатор сущности. По сути, это главный идентификатор,
     * по которому строятся все отношения в файлах XML между сущностями.
     */
    @XmlAttribute(name = "OBJECTID")
    private String objectId;
    /**
     * ФИАС мун. образования
     */
    @XmlAttribute(name = "OBJECTGUID")
    private String fias;
    // TODO: 29.02.2024 Убрать параметр changedId
    /**
     * Идентификатор изменений (По сути тоже не нужен)
     */
    @XmlAttribute(name = "CHANGEID")
    private String changedId;
    /**
     * Наименование мун. образования
     */
    @XmlAttribute(name = "NAME")
    private String name;
    /**
     * Тип мун. образования
     */
    @XmlAttribute(name = "TYPENAME")
    private String type;
    /**
     * Лвл мун. образования
     */
    @XmlAttribute(name = "LEVEL")
    private String level;
    // TODO: 29.02.2024 Убрать параметр nextId
    /**
     * Скорее всего, идентификатор ребенка, например городской округ для области. Тоже не используется.
     */
    @XmlAttribute(name = "NEXTID")
    private String nextId;
    // TODO: 29.02.2024 Убрать параметр isActual
    /**
     * Данные актуальны? 1 - да, 0 - нет
     */
    @XmlAttribute(name = "ISACTUAL")
    private String isActual;
    /**
     * МО еще существует? 1 - да, 0 - нет. Важный пункт, но в данных от ФИАС есть неправильные данные,
     * тут три варианта: либо ошибается ФИАС, либо ОСМ, либо алгоритм не совсем верный
     */
    @XmlAttribute(name = "ISACTIVE")
    private String isActive;
}
