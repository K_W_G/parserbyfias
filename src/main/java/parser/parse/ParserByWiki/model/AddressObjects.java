package parser.parse.ParserByWiki.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * Класс, использующийся для хранения массива сущностей, которые хранят общую информацию о мун. образованиях
 */
@XmlRootElement(name = "ADDRESSOBJECTS")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AddressObjects {
    /**
     * Массив объектов, которые хранят общую информацию о мун. образованиях
     *
     * @see AddresObjectClass
     */
    @XmlElement(name = "OBJECT")
    private List<AddresObjectClass> objects;
}
