package parser.parse.ParserByWiki.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;

/**
 * Класс, использующийся для парсинга файла с началом AS_ADM_HIERARCHY_ и AS_MUN_HIERARCHY_.
 * Содержит параметры, соответствующие наименования в каждой сущности ITEM в файле XML
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@XmlAccessorType(XmlAccessType.FIELD)
public class AdmHierarchyClass {
    // TODO: 29.02.2024 Убрать параметр id
    /**
     * Идентификатор сущности (По сути не нужен)
     */
    @XmlAttribute(name = "ID")
    private String id;
    /**
     * Объектный идентификатор сущности. По сути, это главный идентификатор,
     * по которому строятся все отношения в файлах XML между сущностями.
     */
    @XmlAttribute(name = "OBJECTID")
    private String objectId;
    /**
     * Объектный идентификатор родителя сущности. Используется для поиска родительского ФИАСА мун. образования
     */
    @XmlAttribute(name = "PARENTOBJID")
    private String parentObjectId;

    /**
     * Родительская цепочка
     * Возможные значения:
     * - Область, район, сельское поселение, поселение;
     * - Область, район, городское поселение, поселение;
     * - Область, городской округ, поселение;
     * и т.д. (смотреть в БД после парса XML)
     * При этом, все представлено в виде OBJECTID, т.е. как пример "435345.232323.43343424.2232323"
     */
    @XmlAttribute(name = "PATH")
    private String path;

    /**
     * МО еще существует? 1 - да, 0 - нет. Важный пункт, но в данных от ФИАС есть неправильные данные,
     * тут три варианта: либо ошибается ФИАС, либо ОСМ, либо алгоритм не совсем верный
     */
    @XmlAttribute(name = "ISACTIVE")
    private String isActive;
}
