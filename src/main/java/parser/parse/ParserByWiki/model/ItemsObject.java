package parser.parse.ParserByWiki.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * Класс, использующийся для хранения массива сущностей, которые хранят информацию о взаимоотношениях
 * мун. образований, об их связке друг с другом
 */
@XmlRootElement(name = "ITEMS")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ItemsObject {
    /**
     * Массив объектов, которые хранят информацию о взаимоотношениях
     * мун. образований, об их связке друг с другом
     *
     * @see AdmHierarchyClass
     */
    @XmlElement(name = "ITEM")
    private List<AdmHierarchyClass> objects;
}