package parser.parse.ParserByWiki.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.locationtech.jts.geom.Point;

import javax.persistence.*;

/**
 * Класс, содержащий информацию о мун. образованиях
 */
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "municipal_udmurtia")
public class Municipal {
    /**
     * Идентификатор сущности
     */
    @Id
    @SequenceGenerator(name = "MUN_UDMURTIA_ID_GENERATOR", sequenceName = "municipal_udmurtia_id_seq", allocationSize = 3000)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "MUN_UDMURTIA_ID_GENERATOR")
    private Integer id;
    /**
     * Наименование мун. образования
     */
    private String name;
    /**
     * Тип мун. образования (По факту, 50 из 3000 записей могут содержать неправильный тип.
     * Это виноват ФИАС и те данные, которые они закидывают в XML файлы). Данное поле используется только в switch case
     * типов сущности по пути /getHamlet
     */
    private String type;
    /**
     * ФИАС мун. образования
     */
    private String fias;
    /**
     * ФИАС родительского мун. образования. Добавлен для реализации создания отношений между
     * мун. образованиями во время загрузки данных вв основную БД
     */
    @Column(name = "parent_fias")
    private String parentFias;
    /**
     * Уровень мун. образования. Предполагалось, что уровень можно будет использовать
     * при обращении к АПИ OSM, но там другая система лвл
     */
    @Column(name = "level_osm")
    private Integer levelOSM;
    /**
     * Долгота
     */
    private Float longitude;
    /**
     * Широта
     */
    private Float latitude;

    /**
     * Родительская цепочка
     * Возможные значения:
     * - Область, район, сельское поселение, поселение;
     * - Область, район, городское поселение, поселение;
     * - Область, городской округ, поселение;
     * и т.д. (смотреть в БД после парса XML)
     * При этом поселение - город, село, деревня, кордон и тд
     */
    private String path;
    /**
     * Поле, которое содержит данные о координатах мун. образования. На текущий момент времени содержит
     * координаты точек в виде WKT:
     * - POINT (широта долгота)
     * Возможно в будущем будет добавлена возможность получать координаты районов, округов, сельских и
     * городских поселений, самой области, края и тд в формате WKT MULTIPOLYGON
     */
    private String point;

    @Column(name = "point_wkt")
    private Point pointWKT;

    private Integer population;

    @Column(name = "i_am_here_there_yet")
    private Boolean checkedValue;

    public Municipal(String name, String type, String fias, String parentFias, Integer levelOSM, String path,
                     Boolean checkedValue) {
        this.name = name;
        this.type = type;
        this.fias = fias;
        this.parentFias = parentFias;
        this.levelOSM = levelOSM;
        this.path = path;
        this.checkedValue = checkedValue;
    }
}
