package parser.parse.ParserByWiki.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * Класс, содержащий информацию о мун. образованиях, которые в процессе работы алгоритма, будут удалены
 * из основной БД и добавлены в дополнительную municipal_deleted
 */
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "municipal_deleted_udmurtia")
public class MunicipalDeleted {
    /**
     * Идентификатор сущности
     */
    @Id
    @SequenceGenerator(name = "MUN_DEL_TEST_ID_GENERATOR", sequenceName = "municipal_deleted_udmurtia_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "MUN_DEL_TEST_ID_GENERATOR")
    private Integer id;
    /**
     * Наименование мун. образования
     */
    private String name;
    /**
     * Тип мун. образования
     */
    private String type;
    /**
     * ФИАС мун. образования
     */
    private String fias;
    /**
     * ФИАС родительского мун. образования
     */
    @Column(name = "parent_fias")
    private String parentFias;
    /**
     * Уровень мун. образования
     */
    @Column(name = "level_osm")
    private Integer levelOSM;
    /**
     * Долгота
     */
    private Float longitude;
    /**
     * Широта
     */
    private Float latitude;

    /**
     * Координаты мун. образования
     */
    private String path;

    public MunicipalDeleted(Municipal municipal) {
        this.name = municipal.getName();
        this.type = municipal.getType();
        this.fias = municipal.getFias();
        this.parentFias = municipal.getParentFias();
        this.levelOSM = municipal.getLevelOSM();
        this.longitude = municipal.getLongitude();
        this.latitude = municipal.getLatitude();
    }
}
