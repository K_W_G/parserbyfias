package parser.parse.ParserByWiki.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import parser.parse.ParserByWiki.model.Municipal;

import java.util.List;

@Repository
public interface RepoMinicipal extends JpaRepository<Municipal, Integer> {

    @Query(value = "FROM Municipal as m WHERE m.type = 'тер. СНТ'")
    List<Municipal> getAllSNT();

    @Query(value = "FROM Municipal as m WHERE (m.longitude is null OR m.latitude is null) AND m.checkedValue = false ")
    List<Municipal> getAllSecond();

    @Query(value = "FROM Municipal as m WHERE m.longitude is NOT null AND m.latitude is NOT null")
    List<Municipal> getAllWhereCoordinatesNotNull();

    @Query("FROM Municipal WHERE type not in ('Респ', 'г.о.', 'м.о.', 'с.п', 'г.п', 'г.п.') AND pointWKT != null AND type = 'г'")
    List<Municipal> getByType();
}
