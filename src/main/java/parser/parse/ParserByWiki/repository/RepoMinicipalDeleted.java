package parser.parse.ParserByWiki.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import parser.parse.ParserByWiki.model.Municipal;
import parser.parse.ParserByWiki.model.MunicipalDeleted;

import java.util.List;

@Repository
public interface RepoMinicipalDeleted extends JpaRepository<MunicipalDeleted, Integer> {
}
