package parser.parse.ParserByWiki.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


import parser.parse.ParserByWiki.model.MunicipalOblastAndMO;

import java.util.List;

@Repository
public interface RepoMunicipalOblastAndMO extends JpaRepository<MunicipalOblastAndMO, Integer> {
    List<MunicipalOblastAndMO> getByType(String type);

    MunicipalOblastAndMO getByFias(String fias);

}
