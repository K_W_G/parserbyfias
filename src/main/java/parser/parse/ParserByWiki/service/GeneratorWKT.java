package parser.parse.ParserByWiki.service;

import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.Point;
import org.springframework.stereotype.Service;
import parser.parse.ParserByWiki.model.Municipal;
import parser.parse.ParserByWiki.repository.RepoMinicipal;

import java.util.List;

@Service
public class GeneratorWKT {
    private RepoMinicipal repoMinicipal;

    public GeneratorWKT(RepoMinicipal repoMinicipal) {
        this.repoMinicipal = repoMinicipal;
    }

    public void generateWKT() {
        List<Municipal> municipalsByCoordinatesNotNull =
                repoMinicipal.getAllWhereCoordinatesNotNull();

        for (Municipal municipal : municipalsByCoordinatesNotNull) {
            municipal.setPoint("POINT (" + municipal.getLatitude() + " " + municipal.getLongitude() + ")");
            municipal.setPointWKT(
                    createPoint(String.valueOf(municipal.getLongitude()), String.valueOf(municipal.getLatitude())));
            repoMinicipal.save(municipal);
        }
    }

    private Point createPoint(String x, String y) {
        Point p = new GeometryFactory().createPoint(new Coordinate(Double.parseDouble(x), Double.parseDouble(y)));
        p.setSRID(4326);
        return p;
    }
}
