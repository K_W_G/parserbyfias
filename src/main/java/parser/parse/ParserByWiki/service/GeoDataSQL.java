package parser.parse.ParserByWiki.service;

import org.springframework.stereotype.Service;
import parser.parse.ParserByWiki.model.Municipal;
import parser.parse.ParserByWiki.model.MunicipalOblastAndMO;
import parser.parse.ParserByWiki.repository.RepoMinicipal;
import parser.parse.ParserByWiki.repository.RepoMunicipalOblastAndMO;

import java.util.List;

@Service
public class GeoDataSQL {
    private RepoMinicipal repoMinicipal;

    private RepoMunicipalOblastAndMO repoMinicipalOblast;

    public GeoDataSQL(RepoMinicipal repoMinicipal, RepoMunicipalOblastAndMO repoMinicipalOblast) {
        this.repoMinicipal = repoMinicipal;
        this.repoMinicipalOblast = repoMinicipalOblast;
    }

    public String getOblast() {
        MunicipalOblastAndMO obl = repoMinicipalOblast.getByType("Респ").get(0);
        return "INSERT INTO public.geo_data(border, fias) VALUES (ST_GeomFromText('SRID=4326;" + obl.getPointWKT() + "'), '" + obl.getFias()  + "');";
    }

    public String getMoAndGo() {
        List<MunicipalOblastAndMO> mo = repoMinicipalOblast.getByType("м.о.");
        String sql = "";
        for (MunicipalOblastAndMO municipalOblastAndMO : mo) {
            sql = sql.concat("INSERT INTO public.geo_data(border, fias) VALUES (ST_GeomFromText('SRID=4326;" +
                    municipalOblastAndMO.getPointWKT() + "'), '" + municipalOblastAndMO.getFias()  + "');\n");
        }
        List<MunicipalOblastAndMO> go = repoMinicipalOblast.getByType("г.о.");
        for (MunicipalOblastAndMO municipalOblastAndMO : go) {
            sql = sql.concat("INSERT INTO public.geo_data(border, fias) VALUES (ST_GeomFromText('SRID=4326;" +
                    municipalOblastAndMO.getPointWKT() + "'), '" + municipalOblastAndMO.getFias()  + "');\n");
        }
        return sql;
    }

    public String getNasel() {
        List<Municipal> municipals = repoMinicipal.getByType();
        String sql = "";
        for (Municipal municipal : municipals) {
            sql = sql.concat("INSERT INTO public.geo_data(administrative_center, fias) VALUES (ST_GeomFromText('SRID=4326;" +
                    municipal.getPointWKT() + "'), '" + municipal.getFias()  + "');\n");
        }
        return sql;
    }
}
