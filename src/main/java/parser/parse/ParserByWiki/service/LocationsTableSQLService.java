package parser.parse.ParserByWiki.service;

import org.springframework.stereotype.Service;
import parser.parse.ParserByWiki.model.Municipal;
import parser.parse.ParserByWiki.model.MunicipalOblastAndMO;
import parser.parse.ParserByWiki.repository.RepoMinicipal;
import parser.parse.ParserByWiki.repository.RepoMunicipalOblastAndMO;

import java.util.List;
@Service
public class LocationsTableSQLService {
    private RepoMinicipal repoMinicipal;

    private RepoMunicipalOblastAndMO repoMinicipalOblast;
    public LocationsTableSQLService(RepoMinicipal repoMinicipal, RepoMunicipalOblastAndMO repoMinicipalOblast) {
        this.repoMinicipal = repoMinicipal;
        this.repoMinicipalOblast = repoMinicipalOblast;
    }

    public String getOblast() {
        MunicipalOblastAndMO obl = repoMinicipalOblast.getByType("Респ").get(0);
        String sql = "INSERT INTO locations(fias, level, name, okato, oktmo, population, type, geo_data_id, parent_id) " +
                "VALUES ('" + obl.getFias() + "', 0, '" + obl.getName() + "', null, null, null, '" + obl.getType() + "', " +
                "(SELECT id FROM public.geo_data WHERE fias = '" + obl.getFias() + "'), null);";
        return sql;
    }

    public String getMoAndGo() {
        List<MunicipalOblastAndMO> mo = repoMinicipalOblast.getByType("м.о.");
        String sql = "";
        for (MunicipalOblastAndMO municipalOblastAndMO : mo) {
            sql = sql.concat("INSERT INTO locations(fias, level, name, okato, oktmo, population, type, geo_data_id, parent_id) " +
                    "VALUES ('" + municipalOblastAndMO.getFias() + "', 1, '" + municipalOblastAndMO.getName()
                    + "', null, null, null, 'р-н', " +
                    "(SELECT id FROM public.geo_data WHERE fias = '" + municipalOblastAndMO.getFias() + "'), " +
                    "(SELECT id FROM public.location WHERE fias = '" + municipalOblastAndMO.getParentFias() + "'));\n");
        }
        List<MunicipalOblastAndMO> go = repoMinicipalOblast.getByType("г.о.");
        for (MunicipalOblastAndMO municipalOblastAndMO : go) {
            sql = sql.concat("INSERT INTO locations(fias, level, name, okato, oktmo, population, type, geo_data_id, parent_id) " +
                    "VALUES ('" + municipalOblastAndMO.getFias() + "', 1, '" + municipalOblastAndMO.getName()
                    + "', null, null, null, 'го', " +
                    "(SELECT id FROM public.geo_data WHERE fias = '" + municipalOblastAndMO.getFias() + "'), " +
                    "(SELECT id FROM public.location WHERE fias = '" + municipalOblastAndMO.getParentFias() + "'));\n");
        }
        return sql;
    }

    public String getNasel() {
        List<Municipal> municipals = repoMinicipal.getByType();
        String sql = "";
        for (Municipal municipal : municipals) {
            MunicipalOblastAndMO municipal1 = repoMinicipalOblast.getByFias(municipal.getParentFias());
            if (municipal1.getType().equals("м.о.") || municipal1.getType().equals("г.о.")) {
                sql = sql.concat("INSERT INTO location(fias, level, name, okato, oktmo, population, type, geo_data_id, parent_id) " +
                        "VALUES ('" + municipal.getFias() + "', 2, '" + municipal.getName()
                        + "', null, null, null, '" + municipal.getType() + "', " +
                        "(SELECT id FROM public.geo_data WHERE fias = '" + municipal.getFias() + "'), " +
                        "(SELECT id FROM public.location WHERE fias = '" + municipal.getParentFias() + "'));\n");
            }
            else {
                sql = sql.concat("INSERT INTO location(fias, level, name, okato, oktmo, population, type, geo_data_id, parent_id) " +
                        "VALUES ('" + municipal.getFias() + "', 2, '" + municipal.getName()
                        + "', null, null, null, '" + municipal.getType() + "', " +
                        "(SELECT id FROM public.geo_data WHERE fias = '" + municipal.getFias() + "'), " +
                        "(SELECT id FROM public.location WHERE fias = '" +
                        "(SELECT parent_fias FROM public.location WHERE fias = '" + municipal.getParentFias() + "')'));\n");
            }
        }
        return sql;
    }
}

