package parser.parse.ParserByWiki.service;


import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import parser.parse.ParserByWiki.model.Municipal;
import parser.parse.ParserByWiki.model.MunicipalDeleted;
import parser.parse.ParserByWiki.repository.RepoMinicipal;
import parser.parse.ParserByWiki.repository.RepoMinicipalDeleted;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Класс сервиса, который используется для взаимодействия с АПИ ОСМ, для получения координат и кол-ва населения
 * для мун. образований
 */
@Service
public class OSMService {
    private RepoMinicipal repoMinicipal;
    private RepoMinicipalDeleted repoMinicipalDeleted;
    private String URL_OSM = "https://overpass-api.de/api/interpreter";

    public OSMService(RepoMinicipal repoMinicipal, RepoMinicipalDeleted repoMinicipalDeleted) {
        this.repoMinicipal = repoMinicipal;
        this.repoMinicipalDeleted = repoMinicipalDeleted;
    }

    /**
     * По сути, получаем все мун. образования из БД, у которых долгота или широта нулевые, проходимся по каждому из них,
     * закидывая сначала в метод обработчик mainMarsrut, если он ничего не вернул, пробуем менять
     * буквы (да, да, есть и такой трабл) и опять же закидываем в метод mainMarsrut.
     */
    public void getPoints(String nameOkrug) throws Exception {
        String rayon = "";
        int count = 0;
        long startTime = System.nanoTime();

        List<Municipal> municipals = repoMinicipal.getAllSecond();
        String returnValue = "";
        Map<Character, Character> charactersRotate = new HashMap<>();
        charactersRotate.put('е', 'ё');
        charactersRotate.put('ё', 'е');
        charactersRotate.put('и', 'ы');
        charactersRotate.put('ы', 'и');
        charactersRotate.put('ш', 'щ');
        charactersRotate.put('щ', 'ш');
        charactersRotate.put('-', ' ');
        charactersRotate.put(' ', '-');
        List<String> generatePath = new ArrayList<>();
        List<String> params;

        for (Municipal municipal : municipals) {
            if (count / 300 == 1) {
                Thread.sleep(600000);
                count = 0;
            }
            String municipalType = municipal.getType();
            params = Arrays.stream(municipal.getPath().split(", ")).collect(Collectors.toList());
            returnValue = mainMarsrut(params, municipal, municipalType, nameOkrug);
            count++;
            if (returnValue.equals("СНТ") || returnValue.equals("Да")) continue;
        }
        count = 0;

        municipals = repoMinicipal.getAllSecond();
        for (Municipal municipal : municipals) {
            String municipalType = municipal.getType();
            params = Arrays.stream(municipal.getPath().split(", ")).collect(Collectors.toList());
            outerLoop:
            for (Map.Entry<Character, Character> rotateValue: charactersRotate.entrySet()) {
                generatePath = generateEtoYoVariants(municipal.getPath(), rotateValue.getKey(), rotateValue.getValue());
                for (String str : generatePath) {
                    if (count / 300 == 1) {
                        Thread.sleep(600000);
                        count = 0;
                    }
                    params = Arrays.stream(str.split(", ")).collect(Collectors.toList());
                    returnValue = mainMarsrut(params, municipal, municipalType, nameOkrug);
                    count++;
                    if (returnValue.equals("СНТ") || returnValue.equals("Да")) break outerLoop;
                }
            }
        }
        long endTime = System.nanoTime();
        long durationInMilliseconds = (endTime - startTime) / 1_000_000;
        System.out.println("Время выполнения кода: " + durationInMilliseconds + " миллисекунд");
    }

    // TODO: 29.02.2024 Метод не будет работать, если заменяемы
    //  символ находиться в начале, из-за того, что есть разница в регистре. Исправить ситуацию
    /**
     * Метод, который используется для получения наименования
     * мун. образования из родительского пути, отправку этого наименования в метод замены и затем создание массива,
     * содержащего строки, которые в свою очередь содержат остальную часть родительского пути + всевозможные варианты
     * наименования.
     * Как пример, замена е на ё, входной путь "Владимирская, Гусь-Хрустальный, Коржачинки, Реевский".
     * Метод вернет {"Владимирская, Гусь-Хрустальный, Коржачинки, Реевский",
     * "Владимирская, Гусь-Хрустальный, Коржачинки, Рёевский",
     * "Владимирская, Гусь-Хрустальный, Коржачинки, Реёвский",
     * "Владимирская, Гусь-Хрустальный, Коржачинки, Рёёвский"}
     *
     * @param word Родительский путь
     * @param beforeChar Символ, который заменяется
     * @param afterChar Символ, на который заменяется
     * @return Массив строк, содержащий измененный путь
     */
    public static List<String> generateEtoYoVariants(String word, char beforeChar, char afterChar) {
        List<String> result = new ArrayList<>();
        List<String> params = Arrays.stream(word.split(", ")).collect(Collectors.toList());
        String lastParam = params.get(params.size() - 1);

        result.add(lastParam);
        generateVariants(lastParam.toCharArray(), 0, result, beforeChar, afterChar);
        List<String> resultEndArray = new ArrayList<>();
        params.remove(params.size() - 1);
        String resultEnd = String.join(", ", params);
        for (String res: result) {
            resultEndArray.add(resultEnd + ", " + res);
        }
        return resultEndArray;
    }

    /**
     * Рекурсивный метод замены букв, передаваемых в параметрах метода,
     * друг на друга и добавление этих замен в массив result
     *
     * @param chars массив символов наименования мун. образования
     * @param index Изначально 0, затем индекс символа, который меняется + 1
     * @param result Массив строк, содержащий варианты замен наименования мун. образования
     * @param beforeChar Символ, который заменяется
     * @param afterChar Символ, на который заменяется
     */
    private static void generateVariants(
            char[] chars,
            int index,
            List<String> result,
            char beforeChar,
            char afterChar)
    {
        for (int i = index; i < chars.length; i++) {
            if (chars[i] == beforeChar) {
                chars[i] = afterChar;
                result.add(new String(chars));
                generateVariants(chars, i + 1, result, beforeChar, afterChar);
                chars[i] = beforeChar;
            }
        }
    }

    /**
     * Метод, используемый для маршрутизации муниципальных образований по
     * определенным методам исходя из типа мун. образования. Эти методы возвращают строку, содержащую
     * специализированный запрос к АПИ СОМ, который отправляется в метод mainLogic, который может вернуть "Да", "Нет".
     * Если ни один из запросов не вернул "Да", запускается метод deleteMarshrut.
     *
     * @param params Массив наименований мун. образований из родительского пути
     * @param municipal Объект муниципального образования, координаты которого мы ищем
     * @param municipalType Тип муниципального образования
     * @return Строка, "Да" - если координаты были получены и сохранены в БД,
     * "СНТ" - если тип является одним из типов СНТ, получение которых ведется в другом сервисе,
     * "Нет" - если координаты не были получены
     */
    public String mainMarsrut(List<String> params, Municipal municipal, String municipalType, String nameOkrug) throws Exception {

        String data = "";
        switch (municipalType) {
            case "п":
            case "м":
            case "х":
            case "х.":
            case "погост":
            case "с":
            case "высел":
            case "п-к":
            case "нп.":
            case "с/п":
            case "н/п":
            case "в-ки":
                if (params.size() == 4) {
                    if (params.get(2).contains("город") || params.get(2).contains("округ")) {
                        data = getHamletOn4ParamGorodOrOkrugV1(params, "", nameOkrug);
                        if (mainLogic(data, municipal).equals("Да")) {
                            return "Да";
                        }
                        data = getHamletOn4ParamGorodOrOkrugV1(params, "поселок ", nameOkrug);
                        if (mainLogic(data, municipal).equals("Да")) {
                            return "Да";
                        }
                        data = getHamletOn4ParamGorodOrOkrugV1(params, "посёлок ", nameOkrug);
                        if (mainLogic(data, municipal).equals("Да")) {
                            return "Да";
                        }
                        data = getHamletOn4ParamGorodOrOkrugV2(params, "", nameOkrug);
                        if (mainLogic(data, municipal).equals("Да")) {
                            return "Да";
                        }
                        data = getHamletOn4ParamGorodOrOkrugV2(params, "поселок ", nameOkrug);
                        if (mainLogic(data, municipal).equals("Да")) {
                            return "Да";
                        }
                        data = getHamletOn4ParamGorodOrOkrugV2(params, "посёлок ", nameOkrug);
                        if (mainLogic(data, municipal).equals("Да")) {
                            return "Да";
                        }
                        data = getHamletOn4ParamGorodOrOkrugV3(params, "", nameOkrug);
                        if (mainLogic(data, municipal).equals("Да")) {
                            return "Да";
                        }
                        data = getHamletOn4ParamGorodOrOkrugV3(params, "поселок ", nameOkrug);
                        if (mainLogic(data, municipal).equals("Да")) {
                            return "Да";
                        }
                        data = getHamletOn4ParamGorodOrOkrugV3(params, "посёлок ", nameOkrug);
                        if (mainLogic(data, municipal).equals("Да")) {
                            return "Да";
                        }
                    } else {
                        data = getHamletOn4ParamOtherV1(params, "", nameOkrug);
                        if (mainLogic(data, municipal).equals("Да")) {
                            return "Да";
                        }
                        data = getHamletOn4ParamOtherV1(params, "посёлок ", nameOkrug);
                        if (mainLogic(data, municipal).equals("Да")) {
                            return "Да";
                        }
                        data = getHamletOn4ParamOtherV1(params, "поселок ", nameOkrug);
                        if (mainLogic(data, municipal).equals("Да")) {
                            return "Да";
                        }
                        data = getHamletOn4ParamOtherV2(params, "", nameOkrug);
                        if (mainLogic(data, municipal).equals("Да")) {
                            return "Да";
                        }
                        data = getHamletOn4ParamOtherV2(params, "посёлок ", nameOkrug);
                        if (mainLogic(data, municipal).equals("Да")) {
                            return "Да";
                        }
                        data = getHamletOn4ParamOtherV2(params, "поселок ", nameOkrug);
                        if (mainLogic(data, municipal).equals("Да")) {
                            return "Да";
                        }
                        data = getHamletOn4ParamOtherV3(params, "", nameOkrug);
                        if (mainLogic(data, municipal).equals("Да")) {
                            return "Да";
                        }
                        data = getHamletOn4ParamOtherV3(params, "посёлок ", nameOkrug);
                        if (mainLogic(data, municipal).equals("Да")) {
                            return "Да";
                        }
                        data = getHamletOn4ParamOtherV3(params, "поселок ", nameOkrug);
                        if (mainLogic(data, municipal).equals("Да")) {
                            return "Да";
                        }
                        data = getHamletOn4ParamOtherV3Hamlet(params, "", nameOkrug);
                        if (mainLogic(data, municipal).equals("Да")) {
                            return "Да";
                        }
                        data = getHamletOn4ParamOtherV3Hamlet(params, "посёлок ", nameOkrug);
                        if (mainLogic(data, municipal).equals("Да")) {
                            return "Да";
                        }
                        data = getHamletOn4ParamOtherV3Hamlet(params, "поселок ", nameOkrug);
                        if (mainLogic(data, municipal).equals("Да")) {
                            return "Да";
                        }
                        data = getHamletOn4ParamOtherV4(params, "", nameOkrug);
                        if (mainLogic(data, municipal).equals("Да")) {
                            return "Да";
                        }
                        data = getHamletOn4ParamOtherV4(params, "посёлок ", nameOkrug);
                        if (mainLogic(data, municipal).equals("Да")) {
                            return "Да";
                        }
                        data = getHamletOn4ParamOtherV4(params, "поселок ", nameOkrug);
                        if (mainLogic(data, municipal).equals("Да")) {
                            return "Да";
                        }
                        data = getHamletOn4ParamOtherV5(params, "", nameOkrug);
                        if (mainLogic(data, municipal).equals("Да")) {
                            return "Да";
                        }
                        data = getHamletOn4ParamOtherV5(params, "посёлок ", nameOkrug);
                        if (mainLogic(data, municipal).equals("Да")) {
                            return "Да";
                        }
                        data = getHamletOn4ParamOtherV5(params, "поселок ", nameOkrug);
                        if (mainLogic(data, municipal).equals("Да")) {
                            return "Да";
                        }
                    }
                } else {
                    if (params.get(1).contains("город") || params.get(1).contains("округ")) {
                        data = getHamletOnOtherParamOnGorodOrOkrugV1(params, "", nameOkrug);
                        if (mainLogic(data, municipal).equals("Да")) {
                            return "Да";
                        }
                        data = getHamletOnOtherParamOnGorodOrOkrugV1(params, "посёлок ", nameOkrug);
                        if (mainLogic(data, municipal).equals("Да")) {
                            return "Да";
                        }
                        data = getHamletOnOtherParamOnGorodOrOkrugV1(params, "поселок ", nameOkrug);
                        if (mainLogic(data, municipal).equals("Да")) {
                            return "Да";
                        }
                        data = getHamletOnOtherParamOnGorodOrOkrugV2(params, "", nameOkrug);
                        if (mainLogic(data, municipal).equals("Да")) {
                            return "Да";
                        }
                        data = getHamletOnOtherParamOnGorodOrOkrugV2(params, "посёлок ", nameOkrug);
                        if (mainLogic(data, municipal).equals("Да")) {
                            return "Да";
                        }
                        data = getHamletOnOtherParamOnGorodOrOkrugV2(params, "поселок ", nameOkrug);
                        if (mainLogic(data, municipal).equals("Да")) {
                            return "Да";
                        }
                        data = getHamletOnOtherParamOnGorodOrOkrugV3(params, "", nameOkrug);
                        if (mainLogic(data, municipal).equals("Да")) {
                            return "Да";
                        }
                        data = getHamletOnOtherParamOnGorodOrOkrugV3(params, "посёлок ", nameOkrug);
                        if (mainLogic(data, municipal).equals("Да")) {
                            return "Да";
                        }
                        data = getHamletOnOtherParamOnGorodOrOkrugV3(params, "поселок ", nameOkrug);
                        if (mainLogic(data, municipal).equals("Да")) {
                            return "Да";
                        }
                        data = getHamletOnOtherParamOnGorodOrOkrugV4(params, "", nameOkrug);
                        if (mainLogic(data, municipal).equals("Да")) {
                            return "Да";
                        }
                        data = getHamletOnOtherParamOnGorodOrOkrugV4(params, "посёлок ", nameOkrug);
                        if (mainLogic(data, municipal).equals("Да")) {
                            return "Да";
                        }
                        data = getHamletOnOtherParamOnGorodOrOkrugV4(params, "поселок ", nameOkrug);
                        if (mainLogic(data, municipal).equals("Да")) {
                            return "Да";
                        }
                        data = getHamletOnOtherParamOnGorodOrOkrugV5(params, "", nameOkrug);
                        if (mainLogic(data, municipal).equals("Да")) {
                            return "Да";
                        }
                        data = getHamletOnOtherParamOnGorodOrOkrugV5(params, "посёлок ", nameOkrug);
                        if (mainLogic(data, municipal).equals("Да")) {
                            return "Да";
                        }
                        data = getHamletOnOtherParamOnGorodOrOkrugV5(params, "поселок ", nameOkrug);
                        if (mainLogic(data, municipal).equals("Да")) {
                            return "Да";
                        }
                        data = getHamletOnOtherParamOnGorodOrOkrugV6(params, "", nameOkrug);
                        if (mainLogic(data, municipal).equals("Да")) {
                            return "Да";
                        }
                        data = getHamletOnOtherParamOnGorodOrOkrugV6(params, "посёлок ", nameOkrug);
                        if (mainLogic(data, municipal).equals("Да")) {
                            return "Да";
                        }
                        data = getHamletOnOtherParamOnGorodOrOkrugV6(params, "поселок ", nameOkrug);
                        if (mainLogic(data, municipal).equals("Да")) {
                            return "Да";
                        }
                    } else {
                        data = getHamletOnOtherParamOnOtherV1(params, "", nameOkrug);
                        if (mainLogic(data, municipal).equals("Да")) {
                            return "Да";
                        }
                        data = getHamletOnOtherParamOnOtherV1(params, "посёлок ", nameOkrug);
                        if (mainLogic(data, municipal).equals("Да")) {
                            return "Да";
                        }
                        data = getHamletOnOtherParamOnOtherV1(params, "поселок ", nameOkrug);
                        if (mainLogic(data, municipal).equals("Да")) {
                            return "Да";
                        }
                        data = getHamletOnOtherParamOnOtherV2(params, "", nameOkrug);
                        if (mainLogic(data, municipal).equals("Да")) {
                            return "Да";
                        }
                        data = getHamletOnOtherParamOnOtherV2(params, "посёлок ", nameOkrug);
                        if (mainLogic(data, municipal).equals("Да")) {
                            return "Да";
                        }
                        data = getHamletOnOtherParamOnOtherV2(params, "поселок ", nameOkrug);
                        if (mainLogic(data, municipal).equals("Да")) {
                            return "Да";
                        }
                        data = getHamletOnOtherParamOnOtherV3(params, "", nameOkrug);
                        if (mainLogic(data, municipal).equals("Да")) {
                            return "Да";
                        }
                        data = getHamletOnOtherParamOnOtherV3(params, "посёлок ", nameOkrug);
                        if (mainLogic(data, municipal).equals("Да")) {
                            return "Да";
                        }
                        data = getHamletOnOtherParamOnOtherV3(params, "поселок ", nameOkrug);
                        if (mainLogic(data, municipal).equals("Да")) {
                            return "Да";
                        }
                    }
                }
                data = deleteMarshrut(params, municipal, nameOkrug);
                if (data.equals("Да")) {
                    return "Да";
                }
                municipal.setCheckedValue(true);
                repoMinicipal.save(municipal);
                break;
            case "кордон":
                if (params.size() == 4) {
                    data = getKordonV1(params, nameOkrug);
                    if (mainLogic(data, municipal).equals("Да")) {
                        return "Да";
                    }
                }
                else {
                    data = getKordonV2(params, nameOkrug);
                    if (mainLogic(data, municipal).equals("Да")) {
                        return "Да";
                    }
                }
                data = deleteMarshrut(params, municipal, nameOkrug);
                if (data.equals("Да")) {
                    return "Да";
                }
                municipal.setCheckedValue(true);
                repoMinicipal.save(municipal);
                break;
            case "д":
                if (params.size() == 4) {
                    if (params.get(2).contains("город") || params.get(2).contains("округ")) {
                        if (params.get(2).contains("округ")) {
                            data = getVillageOn4ParamOnOkrug(params, "", nameOkrug);
                            if (mainLogic(data, municipal).equals("Да")) {
                                return "Да";
                            }
                            data = getVillageOn4ParamOnOkrug(params, "поселок ", nameOkrug);
                            if (mainLogic(data, municipal).equals("Да")) {
                                return "Да";
                            }
                            data = getVillageOn4ParamOnOkrug(params, "посёлок ", nameOkrug);
                            if (mainLogic(data, municipal).equals("Да")) {
                                return "Да";
                            }
                        } else {
                            data = getVillageOn4ParamOnGorod(params, "", nameOkrug);
                            if (mainLogic(data, municipal).equals("Да")) {
                                return "Да";
                            }
                            data = getVillageOn4ParamOnGorod(params, "поселок ", nameOkrug);
                            if (mainLogic(data, municipal).equals("Да")) {
                                return "Да";
                            }
                            data = getVillageOn4ParamOnGorod(params, "посёлок ", nameOkrug);
                            if (mainLogic(data, municipal).equals("Да")) {
                                return "Да";
                            }
                        }
                    } else {
                        data = getVillageOn4ParamV1(params, "", nameOkrug);
                        if (mainLogic(data, municipal).equals("Да")) {
                            return "Да";
                        }
                        data = getVillageOn4ParamV1(params, "поселок ", nameOkrug);
                        if (mainLogic(data, municipal).equals("Да")) {
                            return "Да";
                        }
                        data = getVillageOn4ParamV1(params, "посёлок ", nameOkrug);
                        if (mainLogic(data, municipal).equals("Да")) {
                            return "Да";
                        }
                        data = getVillageOn4ParamV2(params, "", nameOkrug);
                        if (mainLogic(data, municipal).equals("Да")) {
                            return "Да";
                        }
                        data = getVillageOn4ParamV2(params, "поселок ", nameOkrug);
                        if (mainLogic(data, municipal).equals("Да")) {
                            return "Да";
                        }
                        data = getVillageOn4ParamV2(params, "посёлок ", nameOkrug);
                        if (mainLogic(data, municipal).equals("Да")) {
                            return "Да";
                        }
                        data = getVillageOn4ParamV3(params, "", nameOkrug);
                        if (mainLogic(data, municipal).equals("Да")) {
                            return "Да";
                        }
                        data = getVillageOn4ParamV3(params, "поселок ", nameOkrug);
                        if (mainLogic(data, municipal).equals("Да")) {
                            return "Да";
                        }
                        data = getVillageOn4ParamV3(params, "посёлок ", nameOkrug);
                        if (mainLogic(data, municipal).equals("Да")) {
                            return "Да";
                        }
                        data = getVillageOn4ParamV4(params, "", nameOkrug);
                        if (mainLogic(data, municipal).equals("Да")) {
                            return "Да";
                        }
                        data = getVillageOn4ParamV4(params, "поселок ", nameOkrug);
                        if (mainLogic(data, municipal).equals("Да")) {
                            return "Да";
                        }
                        data = getVillageOn4ParamV4(params, "посёлок ", nameOkrug);
                        if (mainLogic(data, municipal).equals("Да")) {
                            return "Да";
                        }
                        data = getVillageOn4ParamV5(params, "", nameOkrug);
                        if (mainLogic(data, municipal).equals("Да")) {
                            return "Да";
                        }
                        data = getVillageOn4ParamV5(params, "поселок ", nameOkrug);
                        if (mainLogic(data, municipal).equals("Да")) {
                            return "Да";
                        }
                        data = getVillageOn4ParamV5(params, "посёлок ", nameOkrug);
                        if (mainLogic(data, municipal).equals("Да")) {
                            return "Да";
                        }
                        data = getVillageOn4ParamV6(params, "", nameOkrug);
                        if (mainLogic(data, municipal).equals("Да")) {
                            return "Да";
                        }

                        data = getVillageOn4ParamV6(params, "поселок ", nameOkrug);
                        if (mainLogic(data, municipal).equals("Да")) {
                            return "Да";
                        }
                        data = getVillageOn4ParamV6(params, "посёлок ", nameOkrug);
                        if (mainLogic(data, municipal).equals("Да")) {
                            return "Да";
                        }
                    }
                } else {
                    if (params.get(1).contains("город") || params.get(1).contains("округ")) {
                        data = getVillageOnOtherParamOnOkrugOrGorodV4(params, "", nameOkrug);
                        if (mainLogic(data, municipal).equals("Да")) {
                            return "Да";
                        }
                        data = getVillageOnOtherParamOnOkrugOrGorodV4(params, "поселок ", nameOkrug);
                        if (mainLogic(data, municipal).equals("Да")) {
                            return "Да";
                        }
                        data = getVillageOnOtherParamOnOkrugOrGorodV4(params, "посёлок ", nameOkrug);
                        if (mainLogic(data, municipal).equals("Да")) {
                            return "Да";
                        }
                    } else {
                        data = getVillageOnOtherParamOnOtherV4(params, "", nameOkrug);
                        if (mainLogic(data, municipal).equals("Да")) {
                            return "Да";
                        }
                        data = getVillageOnOtherParamOnOtherV4(params, "поселок ", nameOkrug);
                        if (mainLogic(data, municipal).equals("Да")) {
                            return "Да";
                        }
                        data = getVillageOnOtherParamOnOtherV4(params, "посёлок ", nameOkrug);
                        if (mainLogic(data, municipal).equals("Да")) {
                            return "Да";
                        }
                        data = getVillageOnOtherParamOnOtherV5(params, "", nameOkrug);
                        if (mainLogic(data, municipal).equals("Да")) {
                            return "Да";
                        }
                        data = getVillageOnOtherParamOnOtherV5(params, "поселок ", nameOkrug);
                        if (mainLogic(data, municipal).equals("Да")) {
                            return "Да";
                        }
                        data = getVillageOnOtherParamOnOtherV5(params, "посёлок ", nameOkrug);
                        if (mainLogic(data, municipal).equals("Да")) {
                            return "Да";
                        }
                        data = getVillageOnOtherParamOnOtherV6(params, "", nameOkrug);
                        if (mainLogic(data, municipal).equals("Да")) {
                            return "Да";
                        }
                        data = getVillageOnOtherParamOnOtherV6(params, "поселок ", nameOkrug);
                        if (mainLogic(data, municipal).equals("Да")) {
                            return "Да";
                        }
                        data = getVillageOnOtherParamOnOtherV6(params, "посёлок ", nameOkrug);
                        if (mainLogic(data, municipal).equals("Да")) {
                            return "Да";
                        }
                    }
                }
                data = deleteMarshrut(params, municipal, nameOkrug);
                if (data.equals("Да")) {
                    return "Да";
                }
                municipal.setCheckedValue(true);
                repoMinicipal.save(municipal);
                break;
            case "г":
            case "г.":
                if (params.size() == 4) {
                    data = getGorodParam4(params, nameOkrug);
                    if (mainLogic(data, municipal).equals("Да")) {
                        return "Да";
                    }

                } else {
                    if (params.get(1).contains("округ")) {
                        data = getGorodOnOtherParamOnOkrug(params, nameOkrug);
                        if (mainLogic(data, municipal).equals("Да")) {
                            return "Да";
                        }
                    } else {
                        if (params.get(1).contains("район")) {
                            data = getGorodOnOtherParamOnRayon(params, nameOkrug);
                            if (mainLogic(data, municipal).equals("Да")) {
                                return "Да";
                            }

                        } else {
                            data = getGorodOnOtherParamOnOther(params, nameOkrug);
                            if (mainLogic(data, municipal).equals("Да")) {
                                return "Да";
                            }
                        }
                    }
                }
                data = deleteMarshrut(params, municipal, nameOkrug);
                if (data.equals("Да")) {
                    return "Да";
                }
                municipal.setCheckedValue(true);
                repoMinicipal.save(municipal);
                break;
            default:
                return "СНТ";
        }
        return "Нет";
    }

    /**
     * Метод для 4 параметров, для того случая, когда 3 параметр содержит слово "округ" или "город" и в системе
     * ОСМ имеет путь как "область, район, городской округ \\Наименование округа\\, \\Наименование мун. образования\\"
     * с типом "village"
     *
     * @param params Массив наименований мун. образований из родительского пути
     * @param dopParam Дополнительный параметр, который может быть равен либо "", "поселок", "посёлок"
     * @param municipalNAme Наименование области, края, республики
     * @return Строка, содержащая запрос к АПИ ОСМ.
     */
    public String getHamletOn4ParamGorodOrOkrugV1(List<String> params, String dopParam, String municipalNAme) {
        return String.format("area[\"boundary\"=\"administrative\"][\"name\"=\"%s\"]->.region;\n" +
                        "(\n" +
                        "  area(area.region)[\"boundary\"=\"administrative\"][\"name\"=\"%s район\"]->.rayon;\n" +
                        "  area(area.rayon)[\"boundary\"=\"administrative\"][\"admin_level\"=8][\"name\"=\"городской округ %s\"]->.sel;\n" +
                        "  node(area.sel)[\"place\"=\"village\"][\"name\"=\"%s%s\"];\n" +
                        "  \n" +
                        ");out body qt;\n" +
                        ">;\n" +
                        "out skel qt;",
                municipalNAme,
                params.get(1).trim(),
                params.get(2)
                        .replace("город ", "")
                        .replace("округ ", "")
                        .replace("поселок ", "")
                        .replace("Дома ", "").trim(),
                dopParam,
                params.get(3)
                        .replace("им ", "имени ").trim());
    }

    /**
     * Метод для 4 параметров, для того случая, когда 3 параметр содержит слово "округ" или "город" и в системе
     * ОСМ имеет путь как "область, район, городское поселение \\Наименование поселения\\, \\Наименование мун. образования\\"
     * с типом "hamlet"
     *
     * @param params Массив наименований мун. образований из родительского пути
     * @param dopParam Дополнительный параметр, который может быть равен либо "", "поселок", "посёлок"
     * @param municipalNAme Наименование области, края, республики
     * @return Строка, содержащая запрос к АПИ ОСМ.
     */
    public String getHamletOn4ParamGorodOrOkrugV2(List<String> params, String dopParam, String municipalNAme) {
        return String.format("area[\"boundary\"=\"administrative\"][\"name\"=\"%s\"]->.region;\n" +
                        "(\n" +
                        "  area(area.region)[\"boundary\"=\"administrative\"][\"name\"=\"%s район\"]->.rayon;\n" +
                        "  area(area.rayon)[\"boundary\"=\"administrative\"][\"admin_level\"=8][\"name\"=\"городское поселение %s\"]->.sel;\n" +
                        "  node(area.sel)[\"place\"=\"hamlet\"][\"name\"=\"%s%s\"];\n" +
                        "  \n" +
                        ");out body qt;\n" +
                        ">;\n" +
                        "out skel qt;",
                municipalNAme,
                params.get(1).trim(),
                params.get(2)
                        .replace("поселок ", "")
                        .replace("город ", "")
                        .replace("округ ", "").trim(),
                dopParam,
                params.get(3)
                        .replace("им ", "имени ").trim());
    }

    /**
     * Метод для 4 параметров, для того случая, когда 3 параметр содержит слово "округ" или "город" и в системе
     * ОСМ имеет путь как "область, район, городской округ \\Наименование округа\\, \\Наименование мун. образования\\"
     * с типом "hamlet"
     *
     * @param params Массив наименований мун. образований из родительского пути
     * @param dopParam Дополнительный параметр, который может быть равен либо "", "поселок", "посёлок"
     * @param municipalNAme Наименование области, края, республики
     * @return Строка, содержащая запрос к АПИ ОСМ.
     */
    public String getHamletOn4ParamGorodOrOkrugV3(List<String> params, String dopParam, String municipalNAme) {
        return String.format("area[\"boundary\"=\"administrative\"][\"name\"=\"%s\"]->.region;\n" +
                        "(\n" +
                        "  area(area.region)[\"boundary\"=\"administrative\"][\"name\"=\"%s район\"]->.rayon;\n" +
                        "  area(area.rayon)[\"boundary\"=\"administrative\"][\"admin_level\"=8][\"name\"=\"городской округ %s\"]->.sel;\n" +
                        "  node(area.sel)[\"place\"=\"hamlet\"][\"name\"=\"%s%s\"];\n" +
                        "  \n" +
                        ");out body qt;\n" +
                        ">;\n" +
                        "out skel qt;",
                municipalNAme,
                params.get(1).trim(),
                params.get(2)
                        .replace("поселок ", "")
                        .replace("город ", "")
                        .replace("округ ", "").trim(),
                dopParam,
                params.get(3)
                        .replace("им ", "имени ").trim());
    }

    /**
     * Метод для 4 параметров и в системе
     * ОСМ имеет путь как "область, район, \\Наименование поселения\\ сельское поселение, \\Наименование мун. образования\\"
     * с типом "village"
     *
     * @param params Массив наименований мун. образований из родительского пути
     * @param dopParam Дополнительный параметр, который может быть равен либо "", "поселок", "посёлок"
     * @param municipalNAme Наименование области, края, республики
     * @return Строка, содержащая запрос к АПИ ОСМ.
     */
    public String getHamletOn4ParamOtherV1(List<String> params, String dopParam, String municipalNAme) {
        return String.format("area[\"boundary\"=\"administrative\"][\"name\"=\"%s\"]->.region;\n" +
                        "(\n" +
                        "  area(area.region)[\"boundary\"=\"administrative\"][\"name\"=\"%s район\"]->.rayon;\n" +
                        "  area(area.rayon)[\"boundary\"=\"administrative\"][\"admin_level\"=8][\"name\"=\"%s сельское поселение\"]->.sel;\n" +
                        "  node(area.sel)[\"place\"=\"village\"][\"name\"=\"%s%s\"];\n" +
                        "  \n" +
                        ");out body qt;\n" +
                        ">;\n" +
                        "out skel qt;",
                municipalNAme,
                params.get(1).trim(),
                params.get(2)
                        .replace("поселок ", "")
                        .replace("город ", "")
                        .replace("округ ", "").trim(),
                dopParam,
                params.get(3)
                        .replace("им ", "имени ").trim());
    }

    /**
     * Метод для 4 параметров и в системе
     * ОСМ имеет путь как "область, район, сельское поселение \\Наименование поселения\\, \\Наименование мун. образования\\"
     * с типом "village"
     *
     * @param params Массив наименований мун. образований из родительского пути
     * @param dopParam Дополнительный параметр, который может быть равен либо "", "поселок", "посёлок"
     * @param municipalNAme Наименование области, края, республики
     * @return Строка, содержащая запрос к АПИ ОСМ.
     */
    public String getHamletOn4ParamOtherV2(List<String> params, String dopParam, String municipalNAme) {
        return String.format("area[\"boundary\"=\"administrative\"][\"name\"=\"%s\"]->.region;\n" +
                        "(\n" +
                        "  area(area.region)[\"boundary\"=\"administrative\"][\"name\"=\"%s район\"]->.rayon;\n" +
                        "  area(area.rayon)[\"boundary\"=\"administrative\"][\"admin_level\"=8][\"name\"=\"сельское поселение %s\"]->.sel;\n" +
                        "  node(area.sel)[\"place\"=\"village\"][\"name\"=\"%s%s\"];\n" +
                        "  \n" +
                        ");out body qt;\n" +
                        ">;\n" +
                        "out skel qt;",
                municipalNAme,
                params.get(1).trim(),
                params.get(2)
                        .replace("поселок ", "")
                        .replace("город ", "")
                        .replace("округ ", "").trim(),
                dopParam,
                params.get(3)
                        .replace("им ", "имени ").trim());
    }
    /**
     * Метод для 4 параметров и в системе
     * ОСМ имеет путь как "область, район, сельское поселение \\Наименование поселения\\, \\Наименование мун. образования\\"
     * с типом "hamlet"
     *
     * @param params Массив наименований мун. образований из родительского пути
     * @param dopParam Дополнительный параметр, который может быть равен либо "", "поселок", "посёлок"
     * @param municipalNAme Наименование области, края, республики
     * @return Строка, содержащая запрос к АПИ ОСМ.
     */
    public String getHamletOn4ParamOtherV3(List<String> params, String dopParam, String municipalNAme) {
        return String.format("area[\"boundary\"=\"administrative\"][\"name\"=\"%s\"]->.region;\n" +
                        "(\n" +
                        "  area(area.region)[\"boundary\"=\"administrative\"][\"name\"=\"%s район\"]->.rayon;\n" +
                        "  area(area.rayon)[\"boundary\"=\"administrative\"][\"admin_level\"=8][\"name\"=\"сельское поселение %s\"]->.sel;\n" +
                        "  node(area.sel)[\"place\"=\"hamlet\"][\"name\"=\"%s%s\"];\n" +
                        "  \n" +
                        ");out body qt;\n" +
                        ">;\n" +
                        "out skel qt;",
                municipalNAme,
                params.get(1).trim(),
                params.get(2)
                        .replace("поселок ", "")
                        .replace("город ", "")
                        .replace("округ ", "").trim(),
                dopParam,
                params.get(3)
                        .replace("им ", "имени ").trim());
    }

    /**
     * Метод для 4 параметров и в системе
     * ОСМ имеет путь как "область, район, \\Наименование поселения\\ сельское поселение, \\Наименование мун. образования\\"
     * с типом "hamlet"
     *
     * @param params Массив наименований мун. образований из родительского пути
     * @param dopParam Дополнительный параметр, который может быть равен либо "", "поселок", "посёлок"
     * @param municipalNAme Наименование области, края, республики
     * @return Строка, содержащая запрос к АПИ ОСМ.
     */
    public String getHamletOn4ParamOtherV3Hamlet(List<String> params, String dopParam, String municipalNAme) {
        return String.format("area[\"boundary\"=\"administrative\"][\"name\"=\"%s\"]->.region;\n" +
                        "(\n" +
                        "  area(area.region)[\"boundary\"=\"administrative\"][\"name\"=\"%s район\"]->.rayon;\n" +
                        "  area(area.rayon)[\"boundary\"=\"administrative\"][\"admin_level\"=8][\"name\"=\"%s сельское поселение\"]->.sel;\n" +
                        "  node(area.sel)[\"place\"=\"hamlet\"][\"name\"=\"%s%s\"];\n" +
                        "  \n" +
                        ");out body qt;\n" +
                        ">;\n" +
                        "out skel qt;",
                municipalNAme,
                params.get(1).trim(),
                params.get(2)
                        .replace("поселок ", "")
                        .replace("город ", "")
                        .replace("округ ", "").trim(),
                dopParam,
                params.get(3)
                        .replace("им ", "имени ").trim());
    }

    /**
     * Метод для 4 параметров и в системе
     * ОСМ имеет путь как "область, район, городское поселение \\Наименование поселения\\, \\Наименование мун. образования\\"
     * с типом "hamlet"
     *
     * @param params Массив наименований мун. образований из родительского пути
     * @param dopParam Дополнительный параметр, который может быть равен либо "", "поселок", "посёлок"
     * @param municipalNAme Наименование области, края, республики
     * @return Строка, содержащая запрос к АПИ ОСМ.
     */
    public String getHamletOn4ParamOtherV4(List<String> params, String dopParam, String municipalNAme) {
        return String.format("area[\"boundary\"=\"administrative\"][\"name\"=\"%s\"]->.region;\n" +
                        "(\n" +
                        "  area(area.region)[\"boundary\"=\"administrative\"][\"name\"=\"%s район\"]->.rayon;\n" +
                        "  area(area.rayon)[\"boundary\"=\"administrative\"][\"admin_level\"=8][\"name\"=\"городское поселение %s\"]->.sel;\n" +
                        "  node(area.sel)[\"place\"=\"hamlet\"][\"name\"=\"%s%s\"];\n" +
                        "  \n" +
                        ");out body qt;\n" +
                        ">;\n" +
                        "out skel qt;",
                municipalNAme,
                params.get(1).trim(),
                params.get(2)
                        .replace("поселок ", "")
                        .replace("город ", "")
                        .replace("округ ", "").trim(),
                dopParam,
                params.get(3)
                        .replace("им ", "имени ").trim());
    }

    /**
     * Метод для 4 параметров и в системе
     * ОСМ имеет путь как "область, район, городское поселение \\Наименование поселения\\, \\Наименование мун. образования\\"
     * с типом "village"
     *
     * @param params Массив наименований мун. образований из родительского пути
     * @param dopParam Дополнительный параметр, который может быть равен либо "", "поселок", "посёлок"
     * @param municipalNAme Наименование области, края, республики
     * @return Строка, содержащая запрос к АПИ ОСМ.
     */
    public String getHamletOn4ParamOtherV5(List<String> params, String dopParam, String municipalNAme) {
        return String.format("area[\"boundary\"=\"administrative\"][\"name\"=\"%s\"]->.region;\n" +
                        "(\n" +
                        "  area(area.region)[\"boundary\"=\"administrative\"][\"name\"=\"%s район\"]->.rayon;\n" +
                        "  area(area.rayon)[\"boundary\"=\"administrative\"][\"admin_level\"=8][\"name\"=\"городское поселение %s\"]->.sel;\n" +
                        "  node(area.sel)[\"place\"=\"village\"][\"name\"=\"%s%s\"];\n" +
                        "  \n" +
                        ");out body qt;\n" +
                        ">;\n" +
                        "out skel qt;",
                municipalNAme,
                params.get(1).trim(),
                params.get(2)
                        .replace("поселок ", "")
                        .replace("город ", "")
                        .replace("округ ", "").trim(),
                dopParam,
                params.get(3)
                        .replace("им ", "имени ").trim());
    }

    /**
     * Метод для 3 параметров, для того случая, когда 2 параметр содержит слово "округ" или "город" и в системе
     * ОСМ имеет путь как "область, городской округ \\Наименование округа\\, \\Наименование мун. образования\\"
     * с типом "hamlet"
     *
     * @param params Массив наименований мун. образований из родительского пути
     * @param dopParam Дополнительный параметр, который может быть равен либо "", "поселок", "посёлок"
     * @param municipalNAme Наименование области, края, республики
     * @return Строка, содержащая запрос к АПИ ОСМ.
     */
    public String getHamletOnOtherParamOnGorodOrOkrugV1(List<String> params, String dopParam, String municipalNAme) {
        return String.format("area[\"boundary\"=\"administrative\"][\"name\"=\"%s\"]->.region;\n" +
                        "(\n" +
                        "  area(area.region)[\"boundary\"=\"administrative\"][\"name\"=\"городской округ %s\"]->.rayon;\n" +
                        "  node(area.rayon)[\"place\"=\"hamlet\"][\"name\"=\"%s%s\"];\n" +
                        "  \n" +
                        ");out body qt;\n" +
                        ">;\n" +
                        "out skel qt;",
                municipalNAme,
                params.get(1)
                        .replace("город ", "")
                        .replace("округ ", "")
                        .replace("округ ", "").trim(),
                dopParam,
                params.get(2)
                        .replace("им ", "имени ").trim());
    }

    /**
     * Метод для 3 параметров, для того случая, когда 2 параметр содержит слово "округ" или "город" и в системе
     * ОСМ имеет путь как "область, \\Наименование округа\\ городской округ, \\Наименование мун. образования\\"
     * с типом "hamlet"
     *
     * @param params Массив наименований мун. образований из родительского пути
     * @param dopParam Дополнительный параметр, который может быть равен либо "", "поселок", "посёлок"
     * @param municipalNAme Наименование области, края, республики
     * @return Строка, содержащая запрос к АПИ ОСМ.
     */
    public String getHamletOnOtherParamOnGorodOrOkrugV2(List<String> params, String dopParam, String municipalNAme) {
        return String.format("area[\"boundary\"=\"administrative\"][\"name\"=\"%s\"]->.region;\n" +
                        "(\n" +
                        "  area(area.region)[\"boundary\"=\"administrative\"][\"name\"=\"%s городской округ\"]->.rayon;\n" +
                        "  node(area.rayon)[\"place\"=\"hamlet\"][\"name\"=\"%s%s\"];\n" +
                        "  \n" +
                        ");out body qt;\n" +
                        ">;\n" +
                        "out skel qt;",
                municipalNAme,
                params.get(1)
                        .replace("город ", "")
                        .replace("округ ", "")
                        .replace("округ ", "").trim(),
                dopParam,
                params.get(2)
                        .replace("им ", "имени ").trim());
    }

    /**
     * Метод для 3 параметров, для того случая, когда 2 параметр содержит слово "округ" или "город" и в системе
     * ОСМ имеет путь как "область, \\Наименование округа\\ городской округ, \\Наименование мун. образования\\"
     * с типом "hamlet"
     *
     * @param params Массив наименований мун. образований из родительского пути
     * @param dopParam Дополнительный параметр, который может быть равен либо "", "поселок", "посёлок"
     * @param municipalNAme Наименование области, края, республики
     * @return Строка, содержащая запрос к АПИ ОСМ.
     */
    public String getHamletOnOtherParamOnGorodOrOkrugV3(List<String> params, String dopParam, String municipalNAme) {
        return String.format("area[\"boundary\"=\"administrative\"][\"name\"=\"%s\"]->.region;\n" +
                        "(\n" +
                        "  area(area.region)[\"boundary\"=\"administrative\"][\"name\"=\"%s городской округ\"]->.rayon;\n" +
                        "  node(area.rayon)[\"place\"=\"village\"][\"name\"=\"%s%s\"];\n" +
                        "  \n" +
                        ");out body qt;\n" +
                        ">;\n" +
                        "out skel qt;",
                municipalNAme,
                params.get(1)
                        .replace("город ", "")
                        .replace("округ ", "")
                        .replace("округ ", "").trim(),
                dopParam,
                params.get(2)
                        .replace("им ", "имени ").trim());
    }

    /**
     * Метод для 3 параметров, для того случая, когда 2 параметр содержит слово "округ" или "город" и в системе
     * ОСМ имеет путь как "область, \\Наименование округа\\ городской округ, \\Наименование мун. образования\\"
     * с типом "village"
     *
     * @param params Массив наименований мун. образований из родительского пути
     * @param dopParam Дополнительный параметр, который может быть равен либо "", "поселок", "посёлок"
     * @param municipalNAme Наименование области, края, республики
     * @return Строка, содержащая запрос к АПИ ОСМ.
     */
    public String getHamletOnOtherParamOnGorodOrOkrugV4(List<String> params, String dopParam, String municipalNAme) {
        return String.format("area[\"boundary\"=\"administrative\"][\"name\"=\"%s\"]->.region;\n" +
                        "(\n" +
                        "  area(area.region)[\"boundary\"=\"administrative\"][\"name\"=\"городской округ %s\"]->.rayon;\n" +
                        "  node(area.rayon)[\"place\"=\"village\"][\"name\"=\"%s%s\"];\n" +
                        "  \n" +
                        ");out body qt;\n" +
                        ">;\n" +
                        "out skel qt;",
                municipalNAme,
                params.get(1)
                        .replace("город ", "")
                        .replace("округ ", "")
                        .replace("округ ", "").trim(),
                dopParam,
                params.get(2)
                        .replace("им ", "имени ").trim());
    }

    /**
     * Метод для 3 параметров, для того случая, когда 2 параметр содержит слово "округ" или "город" и в системе
     * ОСМ имеет путь как "область, \\Наименование округа\\ городской округ, \\Наименование мун. образования\\"
     * с типом "suburb"
     *
     * @param params Массив наименований мун. образований из родительского пути
     * @param dopParam Дополнительный параметр, который может быть равен либо "", "поселок", "посёлок"
     * @param municipalNAme Наименование области, края, республики
     * @return Строка, содержащая запрос к АПИ ОСМ.
     */
    public String getHamletOnOtherParamOnGorodOrOkrugV5(List<String> params, String dopParam, String municipalNAme) {
        return String.format("area[\"boundary\"=\"administrative\"][\"name\"=\"%s\"]->.region;\n" +
                        "(\n" +
                        "  area(area.region)[\"boundary\"=\"administrative\"][\"name\"=\"%s городской округ\"]->.rayon;\n" +
                        "  node(area.rayon)[\"place\"=\"suburb\"][\"name\"=\"%s%s\"];\n" +
                        "  \n" +
                        ");out body qt;\n" +
                        ">;\n" +
                        "out skel qt;",
                municipalNAme,
                params.get(1)
                        .replace("город ", "")
                        .replace("округ ", "")
                        .replace("округ ", "").trim(),
                dopParam,
                params.get(2)
                        .replace("им ", "имени ").trim());
    }

    /**
     * Метод для 3 параметров, для того случая, когда 2 параметр содержит слово "округ" или "город" и в системе
     * ОСМ имеет путь как "область, городской округ \\Наименование округа\\, \\Наименование мун. образования\\"
     * с типом "suburb"
     *
     * @param params Массив наименований мун. образований из родительского пути
     * @param dopParam Дополнительный параметр, который может быть равен либо "", "поселок", "посёлок"
     * @param municipalNAme Наименование области, края, республики
     * @return Строка, содержащая запрос к АПИ ОСМ.
     */
    public String getHamletOnOtherParamOnGorodOrOkrugV6(List<String> params, String dopParam, String municipalNAme) {
        return String.format("area[\"boundary\"=\"administrative\"][\"name\"=\"%s\"]->.region;\n" +
                        "(\n" +
                        "  area(area.region)[\"boundary\"=\"administrative\"][\"name\"=\"городской округ %s\"]->.rayon;\n" +
                        "  node(area.rayon)[\"place\"=\"suburb\"][\"name\"=\"%s%s\"];\n" +
                        "  \n" +
                        ");out body qt;\n" +
                        ">;\n" +
                        "out skel qt;",
                municipalNAme,
                params.get(1)
                        .replace("город ", "")
                        .replace("округ ", "")
                        .replace("округ ", "").trim(),
                dopParam,
                params.get(2)
                        .replace("им ", "имени ").trim());
    }

    /**
     * Метод для 3 параметров и в системе
     * ОСМ имеет путь как "область, \\Наименование поселения\\ сельское поселение, \\Наименование мун. образования\\"
     * с типом "hamlet"
     *
     * @param params Массив наименований мун. образований из родительского пути
     * @param dopParam Дополнительный параметр, который может быть равен либо "", "поселок", "посёлок"
     * @param municipalNAme Наименование области, края, республики
     * @return Строка, содержащая запрос к АПИ ОСМ.
     */
    public String getHamletOnOtherParamOnOtherV1(List<String> params, String dopParam, String municipalNAme) {
        return String.format("area[\"boundary\"=\"administrative\"][\"name\"=\"%s\"]->.region;\n" +
                        "(\n" +
                        "  area(area.region)[\"boundary\"=\"administrative\"][\"name\"=\"%s сельское поселение\"]->.rayon;\n" +
                        "  node(area.rayon)[\"place\"=\"hamlet\"][\"name\"=\"%s%s\"];\n" +
                        "  \n" +
                        ");out body qt;\n" +
                        ">;\n" +
                        "out skel qt;",
                municipalNAme,
                params.get(1)
                        .replace("округ ", "")
                        .replace("поселок ", "")
                        .replace("город ", "").trim(),
                dopParam,
                params.get(2)
                        .replace("им ", "имени "));

    }
    /**
     * Метод для 3 параметров и в системе
     * ОСМ имеет путь как "область, \\Наименование района\\, \\Наименование мун. образования\\"
     * с типом "hamlet"
     *
     * @param params Массив наименований мун. образований из родительского пути
     * @param dopParam Дополнительный параметр, который может быть равен либо "", "поселок", "посёлок"
     * @param municipalNAme Наименование области, края, республики
     * @return Строка, содержащая запрос к АПИ ОСМ.
     */
    public String getHamletOnOtherParamOnOtherV2(List<String> params, String dopParam, String municipalNAme) {
        return String.format("area[\"boundary\"=\"administrative\"][\"name\"=\"%s\"]->.region;\n" +
                        "(\n" +
                        "  area(area.region)[\"boundary\"=\"administrative\"][\"name\"=\"%s\"]->.rayon;\n" +
                        "  node(area.rayon)[\"place\"=\"hamlet\"][\"name\"=\"%s%s\"];\n" +
                        "  \n" +
                        ");out body qt;\n" +
                        ">;\n" +
                        "out skel qt;",
                municipalNAme,
                params.get(1)
                        .replace("округ ", "")
                        .replace("поселок ", "")
                        .replace("город ", "").trim(),
                dopParam,
                params.get(2)
                        .replace("им ", "имени ").trim());

    }

    /**
     * Метод для 3 параметров и в системе
     * ОСМ имеет путь как "область, \\Наименование района\\, \\Наименование мун. образования\\"
     * с типом "village"
     *
     * @param params Массив наименований мун. образований из родительского пути
     * @param dopParam Дополнительный параметр, который может быть равен либо "", "поселок", "посёлок"
     * @param municipalNAme Наименование области, края, республики
     * @return Строка, содержащая запрос к АПИ ОСМ.
     */
    public String getHamletOnOtherParamOnOtherV3(List<String> params, String dopParam, String municipalNAme) {
        return String.format("area[\"boundary\"=\"administrative\"][\"name\"=\"%s\"]->.region;\n" +
                        "(\n" +
                        "  area(area.region)[\"boundary\"=\"administrative\"][\"name\"=\"%s\"]->.rayon;\n" +
                        "  node(area.rayon)[\"place\"=\"village\"][\"name\"=\"%s%s\"];\n" +
                        "  \n" +
                        ");out body qt;\n" +
                        ">;\n" +
                        "out skel qt;",
                municipalNAme,
                params.get(1)
                        .replace("округ ", "")
                        .replace("поселок ", "")
                        .replace("город ", "").trim(),
                dopParam,
                params.get(2)
                        .replace("им ", "имени ").trim());

    }

    /**
     * Метод для 4 параметров и в системе
     * ОСМ имеет путь как "область, район, \\Наименование поселения\\ сельское поселение, \\Наименование кордона\\"
     * с типом "hamlet"
     *
     * @param params Массив наименований мун. образований из родительского пути
     * @param municipalNAme Наименование области, края, республики
     * @return Строка, содержащая запрос к АПИ ОСМ.
     */
    public String getKordonV1(List<String> params, String municipalNAme) {
        return String.format("area[\"boundary\"=\"administrative\"][\"name\"=\"%s\"]->.region;\n" +
                        "(\n" +
                        "  area(area.region)[\"boundary\"=\"administrative\"][\"name\"=\"%s район\"]->.rayon;\n" +
                        "  area(area.rayon)[\"boundary\"=\"administrative\"][\"admin_level\"=8][\"name\"=\"%s сельское поселение\"]->.sel;\n" +
                        "  node(area.sel)[\"place\"=\"hamlet\"][\"name\"=\"%s\"];\n" +
                        "  \n" +
                        ");out body qt;\n" +
                        ">;\n" +
                        "out skel qt;",
                municipalNAme,
                params.get(1),
                params.get(2),
                "Кордон " + params.get(3));

    }

    /**
     * Метод для 3 параметров и в системе
     * ОСМ имеет путь как "область, район, \Наименование кордона\
     * с типом "hamlet"
     *
     * @param params Массив наименований мун. образований из родительского пути
     * @param municipalNAme Наименование области, края, республики
     * @return Строка, содержащая запрос к АПИ ОСМ.
     */
    public String getKordonV2(List<String> params, String municipalNAme) {
        return String.format("area[\"boundary\"=\"administrative\"][\"name\"=\"%s\"]->.region;\n" +
                        "(\n" +
                        "  area(area.region)[\"boundary\"=\"administrative\"][\"name\"=\"%s район\"]->.rayon;\n" +
                        "  node(area.sel)[\"place\"=\"hamlet\"][\"name\"=\"%s\"];\n" +
                        "  \n" +
                        ");out body qt;\n" +
                        ">;\n" +
                        "out skel qt;",
                municipalNAme,
                params.get(1),
                "Кордон " + params.get(2));

    }

    /**
     * Метод для 4 параметров и в системе
     * ОСМ имеет путь как "область, \Наименование района\ район, \Наименование го\ городской округ, \Наименование
     * населенного пункта\
     * с типом "hamlet"
     *
     * @param params Массив наименований мун. образований из родительского пути
     * @param dopParam Дополнительный параметр, который может быть равен либо "", "поселок", "посёлок"
     * @param municipalNAme Наименование области, края, республики
     * @return Строка, содержащая запрос к АПИ ОСМ.
     */
    public String getVillageOn4ParamOnOkrug(List<String> params, String dopParam, String municipalNAme) {
        return String.format("area[\"boundary\"=\"administrative\"][\"name\"=\"%s\"]->.region;\n" +
                        "(\n" +
                        "  area(area.region)[\"boundary\"=\"administrative\"][\"name\"=\"%s район\"]->.rayon;\n" +
                        "  area(area.rayon)[\"boundary\"=\"administrative\"][\"admin_level\"=8][\"name\"=\"городской округ %s\"]->.sel;\n" +
                        "  node(area.sel)[\"place\"=\"hamlet\"][\"name\"=\"%s%s\"];\n" +
                        "  \n" +
                        ");out body qt;\n" +
                        ">;\n" +
                        "out skel qt;",
                municipalNAme,
                params.get(1).trim(),
                params.get(2)
                        .replace("город ", "")
                        .replace("округ ", "")
                        .replace("поселок ", "")
                        .replace("Дома ", "").trim(),
                dopParam,
                params.get(3)
                        .replace("им ", "имени ").trim());
    }

    /**
     * Метод для 4 параметров и в системе
     * ОСМ имеет путь как "область, \Наименование района\ район, \Наименование го\ городской округ, \Наименование
     * населенного пункта\
     * с типом "hamlet"
     *
     * @param params Массив наименований мун. образований из родительского пути
     * @param dopParam Дополнительный параметр, который может быть равен либо "", "поселок", "посёлок"
     * @param municipalNAme Наименование области, края, республики
     * @return Строка, содержащая запрос к АПИ ОСМ.
     */
    public String getVillageOn4ParamOnGorod(List<String> params, String dopParam, String municipalNAme) {
        return String.format("area[\"boundary\"=\"administrative\"][\"name\"=\"%s\"]->.region;\n" +
                        "(\n" +
                        "  area(area.region)[\"boundary\"=\"administrative\"][\"name\"=\"%s район\"]->.rayon;\n" +
                        "  area(area.rayon)[\"boundary\"=\"administrative\"][\"admin_level\"=8][\"name\"=\"городское поселение %s\"]->.sel;\n" +
                        "  node(area.sel)[\"place\"=\"hamlet\"][\"name\"=\"%s%s\"];\n" +
                        "  \n" +
                        ");out body qt;\n" +
                        ">;\n" +
                        "out skel qt;",
                municipalNAme,
                params.get(1).trim(),
                params.get(2)
                        .replace("город ", "")
                        .replace("округ ", "")
                        .replace("поселок ", "").replace("Дома ", "").trim(),
                dopParam,
                params.get(3)
                        .replace("им ", "имени ").trim());
    }

    public String getVillageOn4ParamV1(List<String> params, String dopParam, String municipalNAme) {
        return String.format("area[\"boundary\"=\"administrative\"][\"name\"=\"%s\"]->.region;\n" +
                        "(\n" +
                        "  area(area.region)[\"boundary\"=\"administrative\"][\"name\"=\"%s район\"]->.rayon;\n" +
                        "  area(area.rayon)[\"boundary\"=\"administrative\"][\"admin_level\"=8][\"name\"=\"сельское поселение %s\"]->.sel;\n" +
                        "  node(area.sel)[\"place\"=\"hamlet\"][\"name\"=\"%s%s\"];\n" +
                        "  \n" +
                        ");out body qt;\n" +
                        ">;\n" +
                        "out skel qt;",
                municipalNAme,
                params.get(1).trim(),
                params.get(2)
                        .replace("округ ", "")
                        .replace("поселок ", "")
                        .replace("город ", "").trim(),
                dopParam,
                params.get(3)
                        .replace("им ", "имени ").trim());
    }

    public String getVillageOn4ParamV2(List<String> params, String dopParam, String municipalNAme) {
        return String.format("area[\"boundary\"=\"administrative\"][\"name\"=\"%s\"]->.region;\n" +
                        "(\n" +
                        "  area(area.region)[\"boundary\"=\"administrative\"][\"name\"=\"%s район\"]->.rayon;\n" +
                        "  area(area.rayon)[\"boundary\"=\"administrative\"][\"admin_level\"=8][\"name\"=\"%s сельское поселение\"]->.sel;\n" +
                        "  node(area.sel)[\"place\"=\"hamlet\"][\"name\"=\"%s%s\"];\n" +
                        "  \n" +
                        ");out body qt;\n" +
                        ">;\n" +
                        "out skel qt;",
                municipalNAme,
                params.get(1).trim(),
                params.get(2)
                        .replace("округ ", "")
                        .replace("поселок ", "")
                        .replace("город ", "").trim(),
                dopParam,
                params.get(3)
                        .replace("им ", "имени ").trim());
    }

    public String getVillageOn4ParamV3(List<String> params, String dopParam, String municipalNAme) {
        return String.format("area[\"boundary\"=\"administrative\"][\"name\"=\"%s\"]->.region;\n" +
                        "(\n" +
                        "  area(area.region)[\"boundary\"=\"administrative\"][\"name\"=\"%s район\"]->.rayon;\n" +
                        "  area(area.rayon)[\"boundary\"=\"administrative\"][\"admin_level\"=8][\"name\"=\"%s городское поселение\"]->.sel;\n" +
                        "  node(area.sel)[\"place\"=\"hamlet\"][\"name\"=\"%s%s\"];\n" +
                        "  \n" +
                        ");out body qt;\n" +
                        ">;\n" +
                        "out skel qt;",
                municipalNAme,
                params.get(1).trim(),
                params.get(2)
                        .replace("округ ", "")
                        .replace("поселок ", "")
                        .replace("город ", "").trim(),
                dopParam,
                params.get(3)
                        .replace("им ", "имени ").trim());
    }

    public String getVillageOn4ParamV4(List<String> params, String dopParam, String municipalNAme) {
        return String.format("area[\"boundary\"=\"administrative\"][\"name\"=\"%s\"]->.region;\n" +
                        "(\n" +
                        "  area(area.region)[\"boundary\"=\"administrative\"][\"name\"=\"%s район\"]->.rayon;\n" +
                        "  area(area.rayon)[\"boundary\"=\"administrative\"][\"admin_level\"=8][\"name\"=\"городское поселение %s\"]->.sel;\n" +
                        "  node(area.sel)[\"place\"=\"hamlet\"][\"name\"=\"%s%s\"];\n" +
                        "  \n" +
                        ");out body qt;\n" +
                        ">;\n" +
                        "out skel qt;",
                municipalNAme,
                params.get(1).trim(),
                params.get(2)
                        .replace("округ ", "")
                        .replace("поселок ", "")
                        .replace("город ", "").trim(),
                dopParam,
                params.get(3)
                        .replace("им ", "имени ").trim());
    }
    public String getVillageOn4ParamV5(List<String> params, String dopParam, String municipalNAme) {
        return String.format("area[\"boundary\"=\"administrative\"][\"name\"=\"%s\"]->.region;\n" +
                        "(\n" +
                        "  area(area.region)[\"boundary\"=\"administrative\"][\"name\"=\"%s район\"]->.rayon;\n" +
                        "  area(area.rayon)[\"boundary\"=\"administrative\"][\"admin_level\"=8][\"name\"=\"сельское поселение %s\"]->.sel;\n" +
                        "  node(area.sel)[\"place\"=\"village\"][\"name\"=\"%s%s\"];\n" +
                        "  \n" +
                        ");out body qt;\n" +
                        ">;\n" +
                        "out skel qt;",
                municipalNAme,
                params.get(1).trim(),
                params.get(2)
                        .replace("округ ", "")
                        .replace("поселок ", "")
                        .replace("город ", "").trim(),
                dopParam,
                params.get(3)
                        .replace("им ", "имени ").trim());
    }

    public String getVillageOn4ParamV6(List<String> params, String dopParam, String municipalNAme) {
        return String.format("area[\"boundary\"=\"administrative\"][\"name\"=\"%s\"]->.region;\n" +
                        "(\n" +
                        "  area(area.region)[\"boundary\"=\"administrative\"][\"name\"=\"%s район\"]->.rayon;\n" +
                        "  area(area.rayon)[\"boundary\"=\"administrative\"][\"admin_level\"=8][\"name\"=\"%s сельское поселение\"]->.sel;\n" +
                        "  node(area.sel)[\"place\"=\"village\"][\"name\"=\"%s%s\"];\n" +
                        "  \n" +
                        ");out body qt;\n" +
                        ">;\n" +
                        "out skel qt;",
                municipalNAme,
                params.get(1).trim(),
                params.get(2)
                        .replace("округ ", "")
                        .replace("поселок ", "")
                        .replace("город ", "").trim(),
                dopParam,
                params.get(3)
                        .replace("им ", "имени ").trim());
    }

    public String getVillageOnOtherParamOnOkrugOrGorodV4(List<String> params,  String dopParam, String municipalNAme) {
        return String.format("area[\"boundary\"=\"administrative\"][\"name\"=\"%s\"]->.region;\n" +
                        "(\n" +
                        "  area(area.region)[\"boundary\"=\"administrative\"][\"name\"=\"городской округ %s\"]->.rayon;\n" +
                        "  node(area.rayon)[\"place\"=\"hamlet\"][\"name\"=\"%s%s\"];\n" +
                        "  \n" +
                        ");out body qt;\n" +
                        ">;\n" +
                        "out skel qt;",
                municipalNAme,
                params.get(1)
                        .replace("город ", "")
                        .replace("округ ", "")
                        .replace("поселок ", "").replace("Дома ", "").trim(),
                dopParam,
                params.get(2)
                        .replace("им ", "имени ").trim());
    }

    public String getVillageOnOtherParamOnOtherV4(List<String> params,  String dopParam, String municipalNAme) {
        return String.format("area[\"boundary\"=\"administrative\"][\"name\"=\"%s\"]->.region;\n" +
                        "(\n" +
                        "  area(area.region)[\"boundary\"=\"administrative\"][\"name\"=\"%s сельское поселение\"]->.rayon;\n" +
                        "  node(area.rayon)[\"place\"=\"hamlet\"][\"name\"=\"%s%s\"];\n" +
                        "  \n" +
                        ");out body qt;\n" +
                        ">;\n" +
                        "out skel qt;",
                municipalNAme,
                params.get(1)
                        .replace("округ ", "")
                        .replace("поселок ", "")
                        .replace("город ", "").trim(),
                dopParam,
                params.get(2)
                        .replace("им ", "имени ").trim());
    }

    public String getVillageOnOtherParamOnOtherV5(List<String> params, String dopParam, String municipalNAme) {
        return String.format("area[\"boundary\"=\"administrative\"][\"name\"=\"%s\"]->.region;\n" +
                        "(\n" +
                        "  area(area.region)[\"boundary\"=\"administrative\"][\"name\"=\"%s\"]->.rayon;\n" +
                        "  node(area.rayon)[\"place\"=\"hamlet\"][\"name\"=\"%s%s\"];\n" +
                        "  \n" +
                        ");out body qt;\n" +
                        ">;\n" +
                        "out skel qt;",
                municipalNAme,
                params.get(1)
                        .replace("округ ", "")
                        .replace("поселок ", "")
                        .replace("город ", "").trim(),
                dopParam,
                params.get(2)
                        .replace("им ", "имени ").trim());

    }

    public String getVillageOnOtherParamOnOtherV6(List<String> params, String dopParam, String municipalNAme) {
        return String.format("area[\"boundary\"=\"administrative\"][\"name\"=\"%s\"]->.region;\n" +
                        "(\n" +
                        "  area(area.region)[\"boundary\"=\"administrative\"][\"name\"=\"%s\"]->.rayon;\n" +
                        "  node(area.rayon)[\"place\"=\"village\"][\"name\"=\"%s%s\"];\n" +
                        "  \n" +
                        ");out body qt;\n" +
                        ">;\n" +
                        "out skel qt;",
                municipalNAme,
                params.get(1)
                        .replace("округ ", "")
                        .replace("поселок ", "")
                        .replace("город ", "").trim(),
                dopParam,
                params.get(2)
                        .replace("им ", "имени ").trim());

    }

    public String getGorodParam4(List<String> params, String municipalNAme) {
        return String.format("area[\"boundary\"=\"administrative\"][\"name\"=\"%s\"]->.region;\n" +
                        "(\n" +
                        "  area(area.region)[\"boundary\"=\"administrative\"][\"name\"=\"%s район\"]->.rayon;\n" +
                        "  area(area.rayon)[\"boundary\"=\"administrative\"][\"name\"=\"городское поселение %s\"]->.sel;\n" +
                        "  node(area.sel)[\"place\"=\"town\"][\"name\"=\"%s\"];\n" +
                        "  \n" +
                        ");\n" +
                        "out body qt;\n" +
                        ">;\n" +
                        "out skel qt;\n",
                municipalNAme,
                params.get(1).trim(),
                params.get(2)
                        .replace("город ", "")
                        .replace("округ ", "")
                        .replace("поселок ", "").replace("Дома ", "").trim(),
                params.get(3).trim());
    }

    public String getGorodOnOtherParamOnOkrug(List<String> params, String municipalNAme) {
        return String.format("area[\"boundary\"=\"administrative\"][\"name\"=\"%s\"]->.region;\n" +
                        "(\n" +
                        "  area(area.region)[\"boundary\"=\"administrative\"][\"name\"=\"городской округ %s\"]->.sel;\n" +
                        "  node(area.sel)[\"place\"=\"town\"][\"name\"=\"%s\"];\n" +
                        "  \n" +
                        ");\n" +
                        "out body qt;\n" +
                        ">;\n" +
                        "out skel qt;\n",
                municipalNAme,
                params.get(1)
                        .replace("город ", "")
                        .replace("округ ", "")
                        .replace("поселок ", "").replace("Дома ", "").trim(),
                params.get(2).trim());
    }

    public String getGorodOnOtherParamOnRayon(List<String> params, String municipalNAme) {
        return String.format("area[\"boundary\"=\"administrative\"][\"name\"=\"%s\"]->.region;\n" +
                        "(\n" +
                        "  area(area.region)[\"boundary\"=\"administrative\"][\"name\"=\"%s район\"]->.sel;\n" +
                        "  node(area.sel)[\"place\"=\"town\"][\"name\"=\"%s\"];\n" +
                        "  \n" +
                        ");\n" +
                        "out body qt;\n" +
                        ">;\n" +
                        "out skel qt;\n",
                municipalNAme,
                params.get(1)
                        .replace("город ", "")
                        .replace("округ ", "")
                        .replace("поселок ", "")
                        .replace("район ", "").trim(),
                params.get(2).trim());
    }

    public String getGorodOnOtherParamOnOther(List<String> params, String municipalNAme) {
        return String.format("area[\"boundary\"=\"administrative\"][\"name\"=\"%s\"]->.region;\n" +
                        "(\n" +
                        "  area(area.region)[\"boundary\"=\"administrative\"][\"name\"=\"городское поселение %s\"]->.sel;\n" +
                        "  node(area.sel)[\"place\"=\"town\"][\"name\"=\"%s\"];\n" +
                        "  \n" +
                        ");\n" +
                        "out body qt;\n" +
                        ">;\n" +
                        "out skel qt;\n",
                municipalNAme,
                params.get(1)
                        .replace("город ", "")
                        .replace("округ ", "")
                        .replace("поселок ", "").replace("Дома ", "").trim(),
                params.get(2).trim());
    }

    public String deleteMarshrut(List<String> params, Municipal municipal, String nameOkrug) throws Exception {
        String data = "";
        if (params.size() == 4) {
            data = checkDeleteOn4ParamV1(params, "", nameOkrug);
            if (deleteLogic(data, municipal).equals("Да")) {
                return "Да";
            }
            data = checkDeleteOn4ParamV2(params, "", nameOkrug);
            if (deleteLogic(data, municipal).equals("Да")) {
                return "Да";
            }
            data = checkDeleteOn4ParamV3(params, "", nameOkrug);
            if (deleteLogic(data, municipal).equals("Да")) {
                return "Да";
            }
            data = checkDeleteOn4ParamV4(params, "", nameOkrug);
            if (deleteLogic(data, municipal).equals("Да")) {
                return "Да";
            }
            data = checkDeleteOn4ParamV5(params, "", nameOkrug);
            if (deleteLogic(data, municipal).equals("Да")) {
                return "Да";
            }
            data = checkDeleteOn4ParamV6(params, "", nameOkrug);
            if (deleteLogic(data, municipal).equals("Да")) {
                return "Да";
            }
        }
        else {
            data = checkDeleteOnOtherParamV1(params, "", nameOkrug);
            if (deleteLogic(data, municipal).equals("Да")) {
                return "Да";
            }
            data = checkDeleteOnOtherParamV2(params, "", nameOkrug);
            if (deleteLogic(data, municipal).equals("Да")) {
                return "Да";
            }
            data = checkDeleteOnOtherParamV3(params, "", nameOkrug);
            if (deleteLogic(data, municipal).equals("Да")) {
                return "Да";
            }
            data = checkDeleteOnOtherParamV4(params, "", nameOkrug);
            if (deleteLogic(data, municipal).equals("Да")) {
                return "Да";
            }
            data = checkDeleteOnOtherParamV5(params, "", nameOkrug);
            if (deleteLogic(data, municipal).equals("Да")) {
                return "Да";
            }
            data = checkDeleteOnOtherParamV6(params, "", nameOkrug);
            if (deleteLogic(data, municipal).equals("Да")) {
                return "Да";
            }
        }
        return "Нет";
    }

    public String checkDeleteOn4ParamV1(List<String> params, String dopParam, String municipalNAme) {
        return String.format("area[\"boundary\"=\"administrative\"][\"name\"=\"%s\"]->.region;\n" +
                        "(\n" +
                        "  area(area.region)[\"boundary\"=\"administrative\"][\"name\"=\"%s район\"]->.rayon;\n" +
                        "  area(area.rayon)[\"boundary\"=\"administrative\"][\"name\"=\"городское поселение %s\"]->.sel;\n" +
                        "  node(area.sel)[\"place\"=\"locality\"][\"oktmo:user\"=\"yes\"][\"name\"=\"%s%s\"];\n" +
                        "  \n" +
                        ");\n" +
                        "out body qt;\n" +
                        ">;\n" +
                        "out skel qt;\n",
                municipalNAme,
                params.get(1).trim(),
                params.get(2)
                        .replace("город ", "")
                        .replace("округ ", "")
                        .replace("поселок ", "").replace("Дома ", "").trim(),
                dopParam,
                params.get(3).trim());
    }
    public String checkDeleteOn4ParamV2(List<String> params, String dopParam, String municipalNAme) {
        return String.format("area[\"boundary\"=\"administrative\"][\"name\"=\"%s\"]->.region;\n" +
                        "(\n" +
                        "  area(area.region)[\"boundary\"=\"administrative\"][\"name\"=\"%s район\"]->.rayon;\n" +
                        "  area(area.rayon)[\"boundary\"=\"administrative\"][\"name\"=\"%s городское поселение\"]->.sel;\n" +
                        "  node(area.sel)[\"place\"=\"locality\"][\"oktmo:user\"=\"yes\"][\"name\"=\"%s%s\"];\n" +
                        "  \n" +
                        ");\n" +
                        "out body qt;\n" +
                        ">;\n" +
                        "out skel qt;\n",
                municipalNAme,
                params.get(1).trim(),
                params.get(2)
                        .replace("город ", "")
                        .replace("округ ", "")
                        .replace("поселок ", "").replace("Дома ", "").trim(),
                dopParam,
                params.get(3).trim());
    }

    public String checkDeleteOn4ParamV3(List<String> params, String dopParam, String municipalNAme) {
        return String.format("area[\"boundary\"=\"administrative\"][\"name\"=\"%s\"]->.region;\n" +
                        "(\n" +
                        "  area(area.region)[\"boundary\"=\"administrative\"][\"name\"=\"%s район\"]->.rayon;\n" +
                        "  area(area.rayon)[\"boundary\"=\"administrative\"][\"name\"=\"сельское поселение %s\"]->.sel;\n" +
                        "  node(area.sel)[\"place\"=\"locality\"][\"oktmo:user\"=\"yes\"][\"name\"=\"%s%s\"];\n" +
                        "  \n" +
                        ");\n" +
                        "out body qt;\n" +
                        ">;\n" +
                        "out skel qt;\n",
                municipalNAme,
                params.get(1).trim(),
                params.get(2)
                        .replace("город ", "")
                        .replace("округ ", "")
                        .replace("поселок ", "").replace("Дома ", "").trim(),
                dopParam,
                params.get(3).trim());
    }
    public String checkDeleteOn4ParamV4(List<String> params, String dopParam, String municipalNAme) {
        return String.format("area[\"boundary\"=\"administrative\"][\"name\"=\"%s\"]->.region;\n" +
                        "(\n" +
                        "  area(area.region)[\"boundary\"=\"administrative\"][\"name\"=\"%s район\"]->.rayon;\n" +
                        "  area(area.rayon)[\"boundary\"=\"administrative\"][\"name\"=\"%s сельское поселение\"]->.sel;\n" +
                        "  node(area.sel)[\"place\"=\"locality\"][\"oktmo:user\"=\"yes\"][\"name\"=\"%s%s\"];\n" +
                        "  \n" +
                        ");\n" +
                        "out body qt;\n" +
                        ">;\n" +
                        "out skel qt;\n",
                municipalNAme,
                params.get(1).trim(),
                params.get(2)
                        .replace("город ", "")
                        .replace("округ ", "")
                        .replace("поселок ", "").replace("Дома ", "").trim(),
                dopParam,
                params.get(3).trim());
    }

    public String checkDeleteOn4ParamV5(List<String> params, String dopParam, String municipalNAme) {
        return String.format("area[\"boundary\"=\"administrative\"][\"name\"=\"%s\"]->.region;\n" +
                        "(\n" +
                        "  area(area.region)[\"boundary\"=\"administrative\"][\"name\"=\"%s район\"]->.rayon;\n" +
                        "  area(area.rayon)[\"boundary\"=\"administrative\"][\"name\"=\"городской округ %s\"]->.sel;\n" +
                        "  node(area.sel)[\"place\"=\"locality\"][\"oktmo:user\"=\"yes\"][\"name\"=\"%s%s\"];\n" +
                        "  \n" +
                        ");\n" +
                        "out body qt;\n" +
                        ">;\n" +
                        "out skel qt;\n",
                municipalNAme,
                params.get(1).trim(),
                params.get(2)
                        .replace("город ", "")
                        .replace("округ ", "")
                        .replace("поселок ", "").replace("Дома ", "").trim(),
                dopParam,
                params.get(3).trim());
    }
    public String checkDeleteOn4ParamV6(List<String> params, String dopParam, String municipalNAme) {
        return String.format("area[\"boundary\"=\"administrative\"][\"name\"=\"%s\"]->.region;\n" +
                        "(\n" +
                        "  area(area.region)[\"boundary\"=\"administrative\"][\"name\"=\"%s район\"]->.rayon;\n" +
                        "  area(area.rayon)[\"boundary\"=\"administrative\"][\"name\"=\"%s городской округ\"]->.sel;\n" +
                        "  node(area.sel)[\"place\"=\"locality\"][\"oktmo:user\"=\"yes\"][\"name\"=\"%s%s\"];\n" +
                        "  \n" +
                        ");\n" +
                        "out body qt;\n" +
                        ">;\n" +
                        "out skel qt;\n",
                municipalNAme,
                params.get(1).trim(),
                params.get(2)
                        .replace("город ", "")
                        .replace("округ ", "")
                        .replace("поселок ", "").replace("Дома ", "").trim(),
                dopParam,
                params.get(3).trim());
    }

    public String checkDeleteOnOtherParamV1(List<String> params, String dopParam, String municipalNAme) {
        return String.format("area[\"boundary\"=\"administrative\"][\"name\"=\"%s\"]->.region;\n" +
                        "(\n" +
                        "  area(area.region)[\"boundary\"=\"administrative\"][\"name\"=\"городское поселение %s\"]->.sel;\n" +
                        "  node(area.sel)[\"place\"=\"locality\"][\"oktmo:user\"=\"yes\"][\"name\"=\"%s%s\"];\n" +
                        "  \n" +
                        ");\n" +
                        "out body qt;\n" +
                        ">;\n" +
                        "out skel qt;\n",
                municipalNAme,
                params.get(1)
                        .replace("город ", "")
                        .replace("округ ", "")
                        .replace("поселок ", "").replace("Дома ", "").trim(),
                dopParam,
                params.get(2).trim());
    }
    public String checkDeleteOnOtherParamV2(List<String> params, String dopParam, String municipalNAme) {
        return String.format("area[\"boundary\"=\"administrative\"][\"name\"=\"%s\"]->.region;\n" +
                        "(\n" +
                        "  area(area.region)[\"boundary\"=\"administrative\"][\"name\"=\"%s городское поселение\"]->.sel;\n" +
                        "  node(area.sel)[\"place\"=\"locality\"][\"oktmo:user\"=\"yes\"][\"name\"=\"%s%s\"];\n" +
                        "  \n" +
                        ");\n" +
                        "out body qt;\n" +
                        ">;\n" +
                        "out skel qt;\n",
                municipalNAme,
                params.get(1)
                        .replace("город ", "")
                        .replace("округ ", "")
                        .replace("поселок ", "").replace("Дома ", "").trim(),
                dopParam,
                params.get(2).trim());
    }

    public String checkDeleteOnOtherParamV3(List<String> params, String dopParam, String municipalNAme) {
        return String.format("area[\"boundary\"=\"administrative\"][\"name\"=\"%s\"]->.region;\n" +
                        "(\n" +
                        "  area(area.region)[\"boundary\"=\"administrative\"][\"name\"=\"сельское поселение %s\"]->.sel;\n" +
                        "  node(area.sel)[\"place\"=\"locality\"][\"oktmo:user\"=\"yes\"][\"name\"=\"%s%s\"];\n" +
                        "  \n" +
                        ");\n" +
                        "out body qt;\n" +
                        ">;\n" +
                        "out skel qt;\n",
                municipalNAme,
                params.get(1)
                        .replace("город ", "")
                        .replace("округ ", "")
                        .replace("поселок ", "").replace("Дома ", "").trim(),
                dopParam,
                params.get(2).trim());
    }
    public String checkDeleteOnOtherParamV4(List<String> params, String dopParam, String municipalNAme) {
        return String.format("area[\"boundary\"=\"administrative\"][\"name\"=\"%s\"]->.region;\n" +
                        "(\n" +
                        "  area(area.region)[\"boundary\"=\"administrative\"][\"name\"=\"%s сельское поселение\"]->.sel;\n" +
                        "  node(area.sel)[\"place\"=\"locality\"][\"oktmo:user\"=\"yes\"][\"name\"=\"%s%s\"];\n" +
                        "  \n" +
                        ");\n" +
                        "out body qt;\n" +
                        ">;\n" +
                        "out skel qt;\n",
                municipalNAme,
                params.get(1)
                        .replace("город ", "")
                        .replace("округ ", "")
                        .replace("поселок ", "").replace("Дома ", "").trim(),
                dopParam,
                params.get(2).trim());
    }

    public String checkDeleteOnOtherParamV5(List<String> params, String dopParam, String municipalNAme) {
        return String.format("area[\"boundary\"=\"administrative\"][\"name\"=\"%s\"]->.region;\n" +
                        "(\n" +
                        "  area(area.region)[\"boundary\"=\"administrative\"][\"name\"=\"городской округ %s\"]->.sel;\n" +
                        "  node(area.sel)[\"place\"=\"locality\"][\"oktmo:user\"=\"yes\"][\"name\"=\"%s%s\"];\n" +
                        "  \n" +
                        ");\n" +
                        "out body qt;\n" +
                        ">;\n" +
                        "out skel qt;\n",
                municipalNAme,
                params.get(1)
                        .replace("город ", "")
                        .replace("округ ", "")
                        .replace("поселок ", "").replace("Дома ", "").trim(),
                dopParam,
                params.get(2).trim());
    }
    public String checkDeleteOnOtherParamV6(List<String> params, String dopParam, String municipalNAme) {
        return String.format("area[\"boundary\"=\"administrative\"][\"name\"=\"%s\"]->.region;\n" +
                        "(\n" +
                        "  area(area.region)[\"boundary\"=\"administrative\"][\"name\"=\"%s городской округ\"]->.sel;\n" +
                        "  node(area.sel)[\"place\"=\"locality\"][\"oktmo:user\"=\"yes\"][\"name\"=\"%s%s\"];\n" +
                        "  \n" +
                        ");\n" +
                        "out body qt;\n" +
                        ">;\n" +
                        "out skel qt;\n",
                municipalNAme,
                params.get(1)
                        .replace("город ", "")
                        .replace("округ ", "")
                        .replace("поселок ", "").replace("Дома ", "").trim(),
                dopParam,
                params.get(2).trim());
    }

    public String mainLogic(String data, Municipal municipal) throws Exception{
        System.out.println(data);
        URL url = new URL(URL_OSM);
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("POST");
        con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");

        con.setDoOutput(true);
        DataOutputStream wr = new DataOutputStream(con.getOutputStream());
        wr.writeBytes("data=" + URLEncoder.encode(data, StandardCharsets.UTF_8));
        wr.flush();
        wr.close();

        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuilder response = new StringBuilder();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document document = builder.parse(new ByteArrayInputStream(response.toString().getBytes()));
            NodeList nodeList = document.getElementsByTagName("node");
            Thread.sleep(2000);
            if (nodeList == null) {
                return "Нет";
            }

            XPathFactory xPathfactory = XPathFactory.newInstance();
            XPath xpath = xPathfactory.newXPath();
            Element element = (Element) nodeList.item(0);
            Float lat = Float.parseFloat(element.getAttribute("lat"));
            Float lon = Float.parseFloat(element.getAttribute("lon"));
            String expression = "//tag[@k='population']/@v";
            String populationValue = (String) xpath.evaluate(expression, document, XPathConstants.STRING);
            if (!populationValue.isEmpty()) {
                municipal.setPopulation(Integer.parseInt(populationValue));
            }
            municipal.setLatitude(lat);
            municipal.setLongitude(lon);
            municipal.setCheckedValue(true);
            repoMinicipal.save(municipal);
            Thread.sleep(3000);
            return "Да";
        } catch (Exception e) {
            Thread.sleep(2000);
            return "Нет";
        }
    }

    public String deleteLogic(String data, Municipal municipal) throws Exception{
        System.out.println(data);
        URL url = new URL(URL_OSM);
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("POST");
        con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");

        con.setDoOutput(true);
        DataOutputStream wr = new DataOutputStream(con.getOutputStream());
        wr.writeBytes("data=" + URLEncoder.encode(data, StandardCharsets.UTF_8));
        wr.flush();
        wr.close();

        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuilder response = new StringBuilder();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document document = builder.parse(new ByteArrayInputStream(response.toString().getBytes()));
            NodeList nodeList = document.getElementsByTagName("node");
            Thread.sleep(2000);
            if (nodeList == null) return "Нет";
            Element element = (Element) nodeList.item(0);
            Float lat = Float.parseFloat(element.getAttribute("lat"));
            Float lon = Float.parseFloat(element.getAttribute("lon"));
            municipal.setLatitude(lat);
            municipal.setLongitude(lon);
            MunicipalDeleted savedMun = new MunicipalDeleted(municipal);
            repoMinicipalDeleted.save(savedMun);
            repoMinicipal.delete(municipal);
            Thread.sleep(3000);
            return "Да";
        } catch (Exception e) {
            Thread.sleep(2000);
            return "Нет";
        }
    }
}
