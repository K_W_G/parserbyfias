package parser.parse.ParserByWiki.service;

import org.springframework.stereotype.Service;
import parser.parse.ParserByWiki.model.*;
import parser.parse.ParserByWiki.repository.RepoMinicipal;
import parser.parse.ParserByWiki.repository.RepoMinicipalDeleted;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class ParserXMLService {
    private RepoMinicipal repoMinicipal;
    private RepoMinicipalDeleted repoMinicipalDeleted;

    private List<AddresObjectClass> filteredArrayAdress = null;
    private List<AdmHierarchyClass> filteredArrayItems = null;

    public ParserXMLService(RepoMinicipal repoMinicipal, RepoMinicipalDeleted repoMinicipalDeleted) {
        this.repoMinicipal = repoMinicipal;
        this.repoMinicipalDeleted = repoMinicipalDeleted;
    }

    /**
     * Метод, который используется для парсинга XML файлов. Основная логика включает автоматический парсинг
     * информации с файлов, используя стандартные инструменты для работы с xml. Затем, если говорить про первую
     * обработку, в итоговую выборку попадают все те мун. образования, тип которых совпадает
     * с указанными типами в includeTypes и, у которых параметр isActive равен 1 (Данные могут быть не верными).
     * -----------------------------------------------------------------------------------------------------------
     * Второй обработчик используется для парсинга файла, который начинается с AS_MUN_HIERARCHY_.
     * По сути, из него мы получаем объектный идентификатор родителя и родительский путь,
     * содержащий так-же объектные идентификаторы. Так же создается массив объектов netVViborke,
     * который содержит все те сущности AddresObjectClass, OBJECTID, которых, не был найден среди объектов в файле AS_MUN_HIERARCHY_
     * -----------------------------------------------------------------------------------------------------------
     * Третий обработчик нужен для прохождения по данным файла, название которого начинается с AS_ADM_HIERARCHY_,
     * а том случае, если netVViborke не равен null. Обчно, этого хватает, чтобы обработать все мун. образования региона.
     */
    public void parseXML() {
        List<AddresObjectClass> netVViborke = null;
        try {
            File file = new File("AS_ADDR_OBJ_20240219_1093e08d-114b-4369-bfa3-f6bfc3796a6c.XML");
            JAXBContext jaxbContext = JAXBContext.newInstance(AddressObjects.class);

            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            AddressObjects addressObjects = (AddressObjects) jaxbUnmarshaller.unmarshal(file);
            Set<String> sets = addressObjects.getObjects().stream().map(AddresObjectClass::getType).collect(Collectors.toSet());
            for (String adrObj : sets) {
                System.out.println(adrObj);
            }

            List<String> includeTypes = new ArrayList<>(Arrays.asList("г.п.", "погост", "нп", "г.о.", "г", "д",
                    "м", "п", "с", "х", "кордон", "р-н", "с.п.", "обл", "пгт", "м.р-н", "снт", "ст", "тер. СНТ",
                    "тер. ДПК", "днп", "тер. ДНП", "тер. СНП", "тер. ТСН", "тер. СПК", "тер. ДНТ", "мкр"));
            List<String> udmurtiaNew = Arrays.asList("х.", "м.о.", "Респ", "высел", "п-к", "в-ки", "г.", "нп.", "с/п",
                    "н/п", "г.о.");
            includeTypes.addAll(udmurtiaNew);
            filteredArrayAdress = addressObjects.getObjects().stream().filter(o -> includeTypes.contains(o.getType()) && (o.getIsActive().equals("1"))).collect(Collectors.toList());
            System.out.println(filteredArrayAdress.size());

        } catch (JAXBException e) {
            e.printStackTrace();
        }

        try {
            File file = new File("AS_MUN_HIERARCHY_20240219_24d7c26e-6fea-4ea1-8881-43f9a00487fb.XML");
            JAXBContext jaxbContext = JAXBContext.newInstance(ItemsObject.class);

            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            ItemsObject itemsObject = (ItemsObject) jaxbUnmarshaller.unmarshal(file);
            if (filteredArrayAdress != null) {
                List<String> objectIdArray = filteredArrayAdress.stream().map(AddresObjectClass::getObjectId).collect(Collectors.toList());
                filteredArrayItems = itemsObject.getObjects().stream().filter(
                                o -> objectIdArray.contains(o.getObjectId()) && (o.getIsActive().equals("1")))
                        .collect(Collectors.toList());

                List<String> checkedObj = filteredArrayItems.stream().map(AdmHierarchyClass::getObjectId).collect(Collectors.toList());
                netVViborke = filteredArrayAdress.stream().filter(o -> !checkedObj.contains(o.getObjectId())).collect(Collectors.toList());
            }
            else  {
                System.out.println(Arrays.toString(new NullPointerException().getStackTrace()));
            }
        } catch (JAXBException e) {
            e.printStackTrace();
        }

        try {
            File file = new File("AS_ADM_HIERARCHY_20240219_98153934-41b8-4030-b46e-a57de44d47a7.XML");
            JAXBContext jaxbContext = JAXBContext.newInstance(ItemsObject.class);

            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            ItemsObject itemsObject = (ItemsObject) jaxbUnmarshaller.unmarshal(file);
            if (netVViborke != null) {
                List<String> objectIdArray = netVViborke.stream().map(AddresObjectClass::getObjectId).collect(Collectors.toList());
                List<AdmHierarchyClass> filteredArrayItemsV2 = itemsObject.getObjects().stream().filter(
                                o -> objectIdArray.contains(o.getObjectId()) && (o.getIsActive().equals("1")))
                        .collect(Collectors.toList());

                filteredArrayItems.addAll(filteredArrayItemsV2);
                System.out.println(filteredArrayItems.size());
            }
            else  {
                System.out.println(Arrays.toString(new NullPointerException().getStackTrace()));
            }
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }

    /**
     * Метод, используется для получения ФИАС родителя по parentObjectId и
     * наименований мун. образований по OBJECTID внутри параметра path + сохранение данных в БД
     */
    public void addData() {
        System.out.println(filteredArrayAdress.size());
        for (AddresObjectClass aoc : filteredArrayAdress) {
            String name = aoc.getName();
            String fias = aoc.getFias();
            String type = aoc.getType();
            String parentFias = null;
            if (!type.equals("м.р-н")) {
                System.out.println(aoc.getObjectId());
                System.out.println(aoc.getName());
                AdmHierarchyClass objInfo = filteredArrayItems.stream().filter(o -> o.getObjectId().equals(aoc.getObjectId())).collect(Collectors.toList()).get(0);
                if (Integer.parseInt(aoc.getLevel()) == getLevelOnType(aoc.getType())) {
                    String parent = objInfo.getParentObjectId();
                    if (parent != null && !parent.equals("0")) {
                        parentFias = filteredArrayAdress.stream().filter(o -> o.getObjectId().equals(parent)).collect(Collectors.toList()).get(0).getFias();
                    }
                    List<String> pathArray = Arrays.stream(objInfo.getPath().split("\\.")).collect(Collectors.toList());
                    String path = "";
                    for (String pth: pathArray) {
                        AddresObjectClass addresObjectClass = filteredArrayAdress.stream().filter(o -> o.getObjectId().equals(pth)).collect(Collectors.toList()).get(0);
                        path = path.concat(addresObjectClass.getName() + ", ");
                    }

                    Municipal mun = new Municipal(name, type, fias, parentFias, Integer.parseInt(aoc.getLevel()), path, false);
                    repoMinicipal.save(mun);
                }
            }
        }
    }

    /**
     * Метод используется для получения лвл в соответствии с типом мун. образования.
     *
     * @param type Тип муниципального образования
     * @return Лвл мун. образования по XML
     */
    public int getLevelOnType(String type) {
        switch (type) {
            case "обл":
            case "Респ":
                return 1;
            case "р-н":
                return 2;
            case "г.о":
            case "г.о.":
            case "м.о":
            case "м.о.":
                return 3;
            case "с.п":
            case "г.п":
            case "г.п.":
                return 4;
            case "г":
            case "г.":
            case "с/п":
                return 5;
            case "погост":
            case "нп":
            case "д":
            case "м":
            case "п":
            case "с":
            case "х":
            case "кордон":
            case "пгт":
            case "ст":
            case "мкр":
            case "х.":
            case "высел":
            case "п-к":
            case "в-ки":
            case "нп.":
                return 6;
            default:
                return 7;
        }
    }
}
